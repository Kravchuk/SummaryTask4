<%@ include file="/WEB-INF/jspf/taglib.jspf"%>
<%@ page import="ua.nure.kravchuk.SummaryTask4.repository.entity.Role"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
</head>
<body>
	<div id="all">
		<header class="top">
			<%@ include file="/WEB-INF/jspf/header.jspf"%>
		</header>

		<%@ include file="/WEB-INF/jspf/nav_bar.jspf"%>

		<%@ include file="/WEB-INF/jspf/message_handler.jspf"%>

		<div id="main">
			
			<div class="middleRow">
					<h1><fmt:message key="index_jsp.welcome" />	</h1>
			</div>
		</div>

		<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>