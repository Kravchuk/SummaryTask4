<%@ include file="/WEB-INF/jspf/taglib.jspf"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
</head>
<body>
	<div id="all">
		<header class="top">
			<%@ include file="/WEB-INF/jspf/header.jspf"%>
		</header>

		<%@ include file="/WEB-INF/jspf/nav_bar.jspf"%>

		<div id="main">

			<c:choose>
				<c:when test="${user == null || userRole == 'ADMIN'}">

					<form id="registration_form" action="controller" method="post">
						<c:choose>
							<c:when test="${userRole == 'ADMIN'}">
								<input type="hidden" name="command" value="addNewLibrarian" />
							</c:when>
							<c:otherwise>
								<input type="hidden" name="command" value="sendRegEmail" />
							</c:otherwise>
						</c:choose>
						<table>
							<caption>
								<fmt:message key="registration_jsp.regForm" />
							</caption>
							<tr>
								<td><fmt:message key="profile_jsp.name" /></td>
								<td><input type="text" name="name" required></td>
							</tr>

							<tr>
								<td><fmt:message key="profile_jsp.surname" />*</td>
								<td><input type="text" name="surname"></td>
							</tr>

							<tr>
								<td><fmt:message key="profile_jsp.phone" />*</td>
								<td><input type="text" name="phone"></td>
							</tr>


							<tr>
								<td><fmt:message key="profile_jsp.country" />*</td>
								<td><input type="text" name="country"></td>
							</tr>

							<tr>
								<td><fmt:message key="profile_jsp.city" />*</td>
								<td><input type="text" name="city"></td>
							</tr>

							<tr>
								<td><fmt:message key="profile_jsp.email" /></td>
								<td><input type="email" name="email" required></td>
							</tr>

							<tr>
								<td><fmt:message key="profile_jsp.login" /></td>
								<td><input type="text" name="login" required></td>
							</tr>

							<tr>
								<td><fmt:message key="registration_jsp.password" /></td>
								<td><input type="password" name="password1" required></td>
							</tr>

							<tr>
								<td><fmt:message key="registration_jsp.passRepeat" /></td>
								<td><input type="password" name="password2" required></td>
							</tr>

							<c:if test="${user != null && userRole == 'ADMIN'}">
								<tr>
									<td><fmt:message key="profile_jsp.role" /></td>
									<td><input type="text" name="role" required readonly
										value="LIBRARIAN"></td>
								</tr>
							</c:if>
						</table>

						<div class="middleRow">
							<p>
								<fmt:message key="registration_jsp.message" />
							</p>
						</div>
						
						<c:if test="${user == null }">
							<div class="middleRow">
								<p>
									<fmt:message key="registration_jsp.registerMessage" />
								</p>
							</div>
						</c:if>

						<div class="middleRow">
							<p>
								<input type="submit" value="<fmt:message key="registration_jsp.register"/>">
							</p>
						</div>
					</form>
				</c:when>
				<c:otherwise>
					<div class="tableRow">
						<h1>
							<fmt:message key="registration_jsp.loggedIn" />
						</h1>
					</div>
				</c:otherwise>
			</c:choose>

			<p />
			<div class="middleRow">
				<a href="javascript:history.go(-1);"><fmt:message
						key="additional.back" /></a>
			</div>
		</div>
		<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>