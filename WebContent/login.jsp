<%@ include file="/WEB-INF/jspf/taglib.jspf"%>
<%@ page import="ua.nure.kravchuk.SummaryTask4.web.recaptcha.*"%>
<%@ page import="ua.nure.kravchuk.SummaryTask4.web.recaptcha.http.*"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
</head>
<body>
	<div id="all">
		<header class="top">
			<%@ include file="/WEB-INF/jspf/header.jspf"%>
		</header>

		<%@ include file="/WEB-INF/jspf/nav_bar.jspf"%>

		<div id="main">

			<div class="middleRow">
				<h2>
					<fmt:message key="account_jspf.signIn" />
					:
				</h2>
			</div>
			<form id="login_form" action="controller" method="post">
				<input type="hidden" name="command" value="login" />
				<div class="middleRow">
					<h2>	<fmt:message key="account_jspf.loginEmail" />:</h2>
					<input name="login">
					<h2>	<fmt:message key="account_jspf.password" />	:</h2>
					<input type="password" name="password" />
				</div>
				<p/>
				<%-- Online captcha <div class="middleRow">
				<%
         			 ReCaptcha c = ReCaptchaFactory.newReCaptcha("6LczdikTAAAAAIzCbtuJEFCszXQLo28WNRWVWpJI", "6LczdikTAAAAAKjlPwKC3o_Za4wBToxL46izmquk", false);
          				out.print(c.createRecaptchaHtml(null, null));
       			 %>
       			 </div> --%>
       			 
       			 <div class="middleRow">
       			 		<img src="stickyImage"/>
       			 </div>

       			 <div class="middleRow">
					<h2><fmt:message key="account_jspf.answer" /> :</h2> <input type="text" name="answer">
					</div>
       			 <p/>
       			 
				<div class="middleRow">
					<input type="submit" value="<fmt:message key="account_jspf.login"/>">
				</div>
			</form>
		</div>

		<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>