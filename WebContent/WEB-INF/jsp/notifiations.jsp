<%@ include file="/WEB-INF/jspf/taglib.jspf"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
</head>
<body>

	<div id="all">
		<header class="top">
			<%@ include file="/WEB-INF/jspf/header.jspf"%>
		</header>

		<%@ include file="/WEB-INF/jspf/nav_bar.jspf"%>

		<div id="main">
			<div class="tableRow">
				<form id="expired_orders_form" action="controller" method="post">
					<input type="hidden" name="command" value="sendNotifications" />
					<input type="hidden" name="begin" value="begin" />
					<input type="submit" value="<fmt:message key="notifications_jsp.expiredOrders"/>">
				</form>
			</div>
		</div>


		<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>