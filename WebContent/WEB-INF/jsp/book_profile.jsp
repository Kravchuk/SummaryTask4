<%@ include file="/WEB-INF/jspf/taglib.jspf"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
</head>
<body>

	<div id="all">
		<header class="top">
			<%@ include file="/WEB-INF/jspf/header.jspf"%>
		</header>

		<%@ include file="/WEB-INF/jspf/nav_bar.jspf"%>

		<div id="main">
			<c:choose>
				<c:when test="${book != null}">
					<c:choose>
						<c:when	test="${user != null && userRole == 'ADMIN'}">
							<%@ include file="/WEB-INF/jspf/book_profile.jspf"%>
						</c:when>
						<c:otherwise>
							<%@ include file="/WEB-INF/jspf/book_profile_view.jspf"%>
						</c:otherwise>
					</c:choose>
					<p/>
					<div class="middleRow">
						<a href="javascript:history.go(-1);"><fmt:message key="additional.back" /></a>
					</div>
				</c:when>
				<c:otherwise>
					<h1>
						<fmt:message key="profile_jsp.error" />
					</h1>
				</c:otherwise>
			</c:choose>
		</div>
		<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>