<%@ include file="/WEB-INF/jspf/taglib.jspf"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
</head>
<body>

	<div id="all">
		<header class="top">
			<%@ include file="/WEB-INF/jspf/header.jspf"%>
		</header>

		<%@ include file="/WEB-INF/jspf/nav_bar.jspf"%>

		<div id="main">
			<c:choose>
			<c:when test="${emails != null && emails.size() > 0}">
				<%@ include file="/WEB-INF/jspf/expired_orders.jspf"%>
			</c:when>
			<c:otherwise>
				<h1>
					<fmt:message key="expired_orders_jspf.noExpOrders" />
				</h1>
			</c:otherwise>
			</c:choose>
		</div>


		<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>