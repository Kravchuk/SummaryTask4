<%@ include file="/WEB-INF/jspf/taglib.jspf"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
</head>
<body>

	<div id="all">
		<header class="top">
			<%@ include file="/WEB-INF/jspf/header.jspf"%>
		</header>

		<%@ include file="/WEB-INF/jspf/nav_bar.jspf"%>

		<div id="main">
			<c:choose>
			
				<c:when test="${books != null}">
					
					<%@ include file="/WEB-INF/jspf/search_bar.jspf"%>
					<p/>
					<c:if test="${cart != null && cart.size() > 0}">
						<%@ include file="/WEB-INF/jspf/cart.jspf"%>
					</c:if>
					<p/>
					<%@ include file="/WEB-INF/jspf/books.jspf"%>

					<c:if test="${userRole == 'ADMIN'}">
						<p />
						<div class="middleRow">
							<a href='/SummaryTask4/controller?command=addNewBook&begin=begin'><fmt:message
									key="books_jspf.newBook" /></a>
						</div>
					</c:if>

				</c:when>

				<c:otherwise>
					<h1>
						<fmt:message key="catalog_jsp.empty" />
					</h1>
				</c:otherwise>
				
			</c:choose>
		</div>

		<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>