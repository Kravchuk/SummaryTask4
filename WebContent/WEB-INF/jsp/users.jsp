<%@ include file="/WEB-INF/jspf/taglib.jspf"%>
<%@ taglib uri="/WEB-INF/phoneTag.tld" prefix="myTag"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
</head>
<body>

	<div id="all">
		<header class="top">
			<%@ include file="/WEB-INF/jspf/header.jspf"%>
		</header>

		<%@ include file="/WEB-INF/jspf/nav_bar.jspf"%>
		
		<div id="main">
			<c:choose>

				<c:when test="${users != null}">
					<%@ include file="/WEB-INF/jspf/users.jspf"%>
				</c:when>

				<c:when test="${librarians != null}">
					<%@ include file="/WEB-INF/jspf/librarians.jspf"%>
					<c:if test="${userRole == 'ADMIN'}">
						<p />
						<div class="middleRow">
							<a href="registration.jsp"><fmt:message	key="users_jspf.newLibrarian" /></a>
						</div>
					</c:if>
				</c:when>

				<c:otherwise>
					<h1>
						<fmt:message key="users_jsp.empty" />
					</h1>
				</c:otherwise>
			</c:choose>

		</div>


		<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>