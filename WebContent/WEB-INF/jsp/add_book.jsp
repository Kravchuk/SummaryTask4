<%@ include file="/WEB-INF/jspf/taglib.jspf"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
</head>
<body>

	<div id="all">
		<header class="top">
			<%@ include file="/WEB-INF/jspf/header.jspf"%>
		</header>

		<%@ include file="/WEB-INF/jspf/nav_bar.jspf"%>

		<div id="main">
			<c:if test="${user != null}">
				<c:choose>
					<c:when test="${userRole == 'ADMIN'}">
						<%@ include file="/WEB-INF/jspf/add_book.jspf"%>
					</c:when>
					<c:otherwise>
						<h1>
							<fmt:message key="profile_jsp.error" />
						</h1>
					</c:otherwise>
				</c:choose>
			</c:if>
		</div>
		<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>