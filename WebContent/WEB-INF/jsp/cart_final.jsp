<%@ include file="/WEB-INF/jspf/taglib.jspf"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
</head>
<body>

	<div id="all">
		<header class="top">
			<%@ include file="/WEB-INF/jspf/header.jspf"%>
		</header>

		<%@ include file="/WEB-INF/jspf/nav_bar.jspf"%>

		<div id="main">
			<c:choose>

				<c:when test="${cart != null && cart.size() > 0}">

					<%@ include file="/WEB-INF/jspf/cart_final.jspf"%>

				</c:when>

				<c:otherwise>
					<h1>
						<fmt:message key="cart_jsp.empty" />
					</h1>
				</c:otherwise>

			</c:choose>
			<p />
			<div class="middleRow">
				<a href="/SummaryTask4/controller?command=getCatalog"><fmt:message key="additional.back" /></a>
			</div>
		</div>

		<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>