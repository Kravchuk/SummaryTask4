<%@ include file="/WEB-INF/jspf/taglib.jspf"%>
<%@ taglib uri="/WEB-INF/phoneTag.tld" prefix="myTag"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
</head>
<body>

	<div id="all">
		<header class="top">
			<%@ include file="/WEB-INF/jspf/header.jspf"%>
		</header>

		<%@ include file="/WEB-INF/jspf/nav_bar.jspf"%>

		<div id="main">
			<c:choose>
				<c:when test="${user1 != null}">
					<%@ include file="/WEB-INF/jspf/user_profile_view.jspf"%>
				</c:when>
				<c:otherwise>
					<h1>
						<fmt:message key="profile_jsp.error" />
					</h1>
				</c:otherwise>
			</c:choose>
			<p />
			<div class="middleRow">
				<a href="javascript:history.go(-1);"><fmt:message
						key="additional.back" /></a>
			</div>
		</div>
		<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>