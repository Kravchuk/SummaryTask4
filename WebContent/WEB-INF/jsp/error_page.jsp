<%@ page isErrorPage="true"%>
<%@ page import="java.io.PrintWriter"%>
<%@ include file="/WEB-INF/jspf/taglib.jspf"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html>
<html>
<c:set var="title" value="Error" scope="page" />
<head>

<%@ include file="/WEB-INF/jspf/head.jspf"%>

</head>

<body>

	<div id="all">
		<header class="top">
			<%@ include file="/WEB-INF/jspf/header.jspf"%>
		</header>

		<%@ include file="/WEB-INF/jspf/nav_bar.jspf"%>


		<div id="main">

			<h2 class="error">
				<fmt:message key="error_page_jsp.error" />
			</h2>
			<%-- this way we obtain an information about an exception (if it has been occurred) --%>
			<c:set var="code"
				value="${requestScope['javax.servlet.error.status_code']}" />
			<c:set var="message"
				value="${requestScope['javax.servlet.error.message']}" />
			<c:set var="exception"
				value="${requestScope['javax.servlet.error.exception']}" />
			<c:if test="${not empty code}">
				<h3>Error code: ${code}</h3>
			</c:if>
			<c:if test="${not empty message}">
				<h3>${message}</h3>
			</c:if>
			<c:if test="${not empty exception}">
				<%
					exception.printStackTrace(new PrintWriter(out));
				%>
			</c:if>
			<%-- if we get this page using forward --%>
			<c:if test="${not empty requestScope.errorMessage}">
				<h3>${requestScope.errorMessage}</h3>
			</c:if>
			<p />
			<div class="middleRow">
				<a href="javascript:history.go(-1);"><fmt:message
						key="additional.back" /></a>
			</div>
		</div>

		<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>