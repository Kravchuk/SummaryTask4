-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
SET NAMES 'utf8';
SET CHARACTER SET utf8;
SET COLLATION_CONNECTION='utf8_general_ci';
-- -----------------------------------------------------
-- Schema library
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema library
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `library`;
USE `library` ;

-- -----------------------------------------------------
-- Table `library`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`users` (
  `user_id` INT NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(45) NULL,
  `password` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `name` VARCHAR(45) NULL,
  `surname` VARCHAR(45) NULL,
  `phone` VARCHAR(15) NULL,
  `country` VARCHAR(45) NULL,
  `city` VARCHAR(45) NULL,
  `registration_date` DATE NULL,
  `is_blocked` TINYINT(1) NULL DEFAULT 0,
  `role` ENUM('user', 'librarian', 'admin') NULL,
  PRIMARY KEY (`user_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `library`.`catalog`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`catalog` (
  `book_id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NULL,
  `author` VARCHAR(60) NULL,
  `publisher` VARCHAR(60) NULL,
  `genre` VARCHAR(45) NULL,
  `publication_date` DATE NULL,
  `total_amount` INT NULL,
  `available` INT NULL,
  `number_of_days_for_use` INT NULL,
  `penalty_payment` DECIMAL(4,2) NULL,
  `number_of_readings` INT NULL DEFAULT 0,
  PRIMARY KEY (`book_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `library`.`orders`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`orders` (
  `order_id` INT NOT NULL AUTO_INCREMENT,
  `return_date` DATE NULL,
  `is_in_reading_room` TINYINT(1) NULL DEFAULT 0,
  `status` ENUM('new_request', 'given_to_user', 'returned') NULL DEFAULT 'new_request',
  `user_id` INT NOT NULL,
  `book_id` INT NOT NULL,
  PRIMARY KEY (`order_id`),
  INDEX `fk_orders_users_idx` (`user_id` ASC),
  INDEX `fk_orders_catalog1_idx` (`book_id` ASC),
  CONSTRAINT `fk_orders_users`
    FOREIGN KEY (`user_id`)
    REFERENCES `library`.`users` (`user_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_orders_catalog1`
    FOREIGN KEY (`book_id`)
    REFERENCES `library`.`catalog` (`book_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
