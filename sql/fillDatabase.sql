INSERT INTO `library`.`users` (`login`, `password`, `email`, `name`, `surname`, `phone`, `country`, `city`, `registration_date`, `role`) 
		
	VALUES  ('user1', '1user', 'user1email', 'user1', 'user111', '+381111111111', 'USA', 'Orlando', '2000.12.02', 'user'),
				
		('user2', '2user', 'user2email', 'user2', 'user222', '+292222222222', 'Russia', 'Vladivostok', '2000.10.22', 'user'),
				
		('юзер3', '3юзер', 'юзер3почта', 'юзер3', 'юзер333', '775544', 'Польша', 'Краков', '2001-10-12', 'user'),
				
		('librarian1', '1librarian', 'librarian1email', 'librarian1', 'librarian111', '+223333333333', 'Portugal', 'Lisbon', '1999.03.01', 'librarian'),
				
		('librarian2', '2librarian', 'librarian2email', 'librarian2', 'librarian222', '+223444444444', 'Italy', 'Rome', '1993.03.01', 'librarian'),
				
		('admin1', '1admin', 'admin1email', 'admin1', 'admin111', '+331111111111', 'Italy', 'Roma', '1999.01.28', 'admin');
				


INSERT INTO `library`.`catalog` (`name`, `author`, `publisher`, `genre`, `publication_date`, `total_amount`, `available`, `number_of_days_for_use`, `penalty_payment`) 
		
	VALUES  ('Противостояние', 'Стивен Кинг', 'Doubleday', 'постапокалиптика', '1978-09-01', 21, 21, 22, 20),
				
		('Harry Potter and the Goblet of Fire', 'J. K. Rowling', 'Bloomsbury', 'novel', '2002.03.21', 13, 13, 20, 30),
				
		('1984', 'Джордж Оруэлл', 'Secker and Warburg', 'роман', '1949-06-16', 7, 7, 22, 33),
				
		('The Lord of the Rings', 'John Ronald Reuel Tolkien', 'George Allen & Unwin', 'fantasy', '1955.04.10', 17, 17, 30, 40),
				
		('One Hundred Years of Solitude', 'Gabriel Garcia Marquez', 'Harper & Row', 'novel', '1967.05.05', 14, 14, 23, 30);
				


INSERT INTO `library`.`orders` (`return_date`, `is_in_reading_room`, `status`, `user_id`, `book_id`) 
		
	VALUES  ('2016.09.09', 0, DEFAULT, 2, 1),
				
		('2016.08.09', 0, DEFAULT, 3, 2),
				
		('2016.09.01', 1, 'given_to_user', 2, 3),
				
		('2016.08.01', 0, 'returned', 2, 2),
				
		('2016.09.10', 1, 'returned', 1, 4),
				
		('2016.09.04', 0, DEFAULT, 3, 5);
			
						   