package ua.nure.kravchuk.SummaryTask4.web.tag.custom;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;


/**
 * Custom tag library tag for converting phone string
 * 
 * @author P.Kravchuk
 *
 */
public class ChangePhoneTag extends TagSupport {

	private static final long serialVersionUID = -7281624622490858416L;

	private String myPhone;

	public void setMyPhone(String myPhone) {
		this.myPhone = myPhone;
	}

	@Override
	public int doStartTag() throws JspException {
		try {
			pageContext.getOut().print(formatPhone(this.myPhone));
		} catch (IOException e) {
			throw new JspException("Error: " + e.getMessage());
		}
		return EVAL_BODY_AGAIN;
	}

	private String formatPhone(String phone) {
		StringBuilder result = new StringBuilder(phone);
		if (result.length() == 13) {
			result.insert(11, "-");
			result.insert(9, "-");
			result.insert(6, ") ");
			result.insert(3, " (");
		}
		
		if (result.length() == 10) {
			result.insert(8, "-");
			result.insert(6, "-");
			result.insert(3, ") ");
			result.insert(0, " (");
		}
		
		if (result.length() == 6) {
			result.insert(4, "-");
			result.insert(2, "-");
		}
		
		if (result.length() == 7) {
			result.insert(5, "-");
			result.insert(3, "-");
		}

		return result.toString();
	}
}
