package ua.nure.kravchuk.SummaryTask4.web.listener;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * Context listener.
 * 
 * @author P.Kravchuk
 * 
 */
public class ContextListener implements ServletContextListener {

	private static final Logger LOG = Logger.getLogger(ContextListener.class);

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		log("Servlet context destruction starts");
		// no op
		log("Servlet context destruction finished");
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {
		log("Servlet context initialization starts");

		ServletContext servletContext = event.getServletContext();
		initLog4J(servletContext);
		initCommandContainer();
		initLocales(servletContext);

		log("Servlet context initialization finished");
	}

	/**
	 * Initializes locales.
	 * 
	 * @param servletContext
	 */
	private void initLocales(ServletContext servletContext) {
		log("Locales initialization started");

		// obtain file name with locales descriptions;
		String localesFileName = servletContext.getInitParameter("locales");

		// obtain reale path on server
		String localesFileRealPath = servletContext.getRealPath(localesFileName);

		// locad descriptions
		Properties locales = new Properties();
		try {
			locales.load(new FileInputStream(localesFileRealPath));
		} catch (IOException e) {
			log("Error in locales context initialization");
			throw new IllegalStateException("Cannot load locales");
		}

		// save descriptions to servlet context
		servletContext.setAttribute("locales", locales);
		locales.list(System.out);

		log("Locales initialization finished");
	}

	/**
	 * Initializes log4j framework.
	 * 
	 * @param servletContext
	 */
	private void initLog4J(ServletContext servletContext) {
		log("Log4J initialization started");

		try {
			PropertyConfigurator.configure(servletContext.getRealPath("WEB-INF/log4j.properties"));
			LOG.debug("Log4j has been initialized");
		} catch (Exception ex) {
			log("Cannot configure Log4j");
			throw new IllegalStateException("Cannot configure Log4j");
		}

		log("Log4J initialization finished");
	}

	/**
	 * Initializes CommandContainer.
	 * 
	 * @param servletContext
	 */
	private void initCommandContainer() {
		try {
			Class.forName("ua.nure.kravchuk.SummaryTask4.web.command.CommandContainer");
		} catch (ClassNotFoundException ex) {
			log("Cannot initialize Command Container");
			throw new IllegalStateException("Cannot initialize Command Container");
		}
	}

	private void log(String msg) {
		System.out.println("[ContextListener] " + msg);
	}
}