package ua.nure.kravchuk.SummaryTask4.web.recaptcha.http;

public interface HttpLoader {

	public String httpPost(String url, String postdata);
	
	public String httpGet(String url);
}
