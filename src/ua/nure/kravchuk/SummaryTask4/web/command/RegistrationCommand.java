package ua.nure.kravchuk.SummaryTask4.web.command;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.exception.AppException;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Role;
import ua.nure.kravchuk.SummaryTask4.repository.entity.User;
import ua.nure.kravchuk.SummaryTask4.sevices.UserService;
import ua.nure.kravchuk.SummaryTask4.util.Constants;

/**
 * Registration command.
 * 
 * @author P.Kravchuk
 * 
 */
public class RegistrationCommand extends Command {

	private static final long serialVersionUID = -3713383132981708852L;

	private static final Logger LOG = Logger.getLogger(RegistrationCommand.class);

	@Override
	public String logic(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		String forward = Constants.Path.ERROR;
		UserService manager = new UserService();
		
		String login = request.getParameter("login");
		LOG.trace("Request parameter: login --> " + login);

		User user1 = manager.findUserByLogin(login);
		if (user1 != null) {
			LOG.error("User with such login is already exist");
			throw new AppException("We already have user with such login.");
		}

		String email = request.getParameter("email");
		LOG.trace("Request parameter: email --> " + email);
		
		User user2 = manager.findUserByEmail(email);
		if (user2 != null) {
			LOG.error("User with such email is already exist");
			throw new AppException("We already have user with such email.");
		}

		String password = request.getParameter("password");
		LOG.trace("Request parameter: password --> " + password);
		String name = request.getParameter("name");
		LOG.trace("Request parameter: name --> " + name);
		String surname = request.getParameter("surname");
		LOG.trace("Request parameter: surname --> " + surname);
		String phone = request.getParameter("phone");
		LOG.trace("Request parameter: phone --> " + phone);
		String country = request.getParameter("country");
		LOG.trace("Request parameter: country --> " + country);
		String city = request.getParameter("city");
		LOG.trace("Request parameter: city --> " + city);
		
		User user = User.createUser(login, password, email, name, surname, phone, country, city, Role.USER);

		boolean isAdded = manager.insertUser(user);

		if (isAdded) {
			LOG.trace("New user: user --> " + user.toString());
			forward = Constants.Path.SEND_REDIRECT;
		} else {
			LOG.error("Cannot insert new user");
			throw new AppException("Cannot insert new user");
		}

		return forward;
	}

	

}
