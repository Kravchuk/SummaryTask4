package ua.nure.kravchuk.SummaryTask4.web.command;

import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.web.command.catalog.AddNewBookCommand;
import ua.nure.kravchuk.SummaryTask4.web.command.catalog.DeleteBookCommand;
import ua.nure.kravchuk.SummaryTask4.web.command.catalog.EditBookCommand;
import ua.nure.kravchuk.SummaryTask4.web.command.catalog.FindBookCommand;
import ua.nure.kravchuk.SummaryTask4.web.command.catalog.FindBooksByParametersCommand;
import ua.nure.kravchuk.SummaryTask4.web.command.catalog.GetCatalogCommand;
import ua.nure.kravchuk.SummaryTask4.web.command.order.AddToCartCommand;
import ua.nure.kravchuk.SummaryTask4.web.command.order.CancelOrderCommand;
import ua.nure.kravchuk.SummaryTask4.web.command.order.DeleteFromCartCommand;
import ua.nure.kravchuk.SummaryTask4.web.command.order.GetAllOrdersCommand;
import ua.nure.kravchuk.SummaryTask4.web.command.order.GetUnconfirmedOrdersCommand;
import ua.nure.kravchuk.SummaryTask4.web.command.order.GetUserOrdersCommand;
import ua.nure.kravchuk.SummaryTask4.web.command.order.GiveBookCommand;
import ua.nure.kravchuk.SummaryTask4.web.command.order.MakeOrderCommand;
import ua.nure.kravchuk.SummaryTask4.web.command.order.PayPenaltyCommand;
import ua.nure.kravchuk.SummaryTask4.web.command.order.ReturnBookCommand;
import ua.nure.kravchuk.SummaryTask4.web.command.user.AddNewLibrarianCommand;
import ua.nure.kravchuk.SummaryTask4.web.command.user.BlockUserCommand;
import ua.nure.kravchuk.SummaryTask4.web.command.user.DeleteLibrarianCommand;
import ua.nure.kravchuk.SummaryTask4.web.command.user.EditUserCommand;
import ua.nure.kravchuk.SummaryTask4.web.command.user.FindUserCommand;
import ua.nure.kravchuk.SummaryTask4.web.command.user.GetUsersCommand;
import ua.nure.kravchuk.SummaryTask4.web.command.user.SendNotificationsCommand;

/**
 * Holder for all commands.
 * 
 * @author P.Kravchuk
 * 
 */
public class CommandContainer {
	
	private static final Logger LOG = Logger.getLogger(CommandContainer.class);
	
	private static Map<String, Command> commands = new TreeMap<String, Command>();
	
	static {
		// common commands
		commands.put("logout", new LogoutCommand());
		commands.put("noCommand", new NoCommand());
		commands.put("findUser", new FindUserCommand());
		
		
		//out of control commands
		commands.put("login", new LoginCommand());
		commands.put("registration", new RegistrationCommand());
		commands.put("getCatalog", new GetCatalogCommand());
		commands.put("sendRegEmail", new SendRegEmailCommand());
		commands.put("findBookByParameter", new FindBooksByParametersCommand());
		commands.put("findBook", new FindBookCommand());
		
		//admin commands
		commands.put("getUsers", new GetUsersCommand());
		commands.put("deleteLib", new DeleteLibrarianCommand());
		commands.put("blockUser", new BlockUserCommand());
		commands.put("editBook", new EditBookCommand());
		commands.put("deleteBook", new DeleteBookCommand());
		commands.put("addNewBook", new AddNewBookCommand());
		commands.put("addNewLibrarian", new AddNewLibrarianCommand());
		commands.put("sendNotifications", new SendNotificationsCommand());
		
		//librarian commands
		commands.put("giveBook", new GiveBookCommand());
		commands.put("getUnconfirmedOrders", new GetUnconfirmedOrdersCommand());
		commands.put("getAllOrders", new GetAllOrdersCommand());
		
		//user commands
		commands.put("getUserOrders", new GetUserOrdersCommand());
		commands.put("makeOrder", new MakeOrderCommand());
		commands.put("cancelOrder", new CancelOrderCommand());
		commands.put("returnBook", new ReturnBookCommand());
		commands.put("payPenalty", new PayPenaltyCommand());
		commands.put("editUser", new EditUserCommand());
		commands.put("addToCart", new AddToCartCommand());
		commands.put("deleteFromCart", new DeleteFromCartCommand());

		
		LOG.debug("Command container was successfully initialized");
		LOG.trace("Number of commands --> " + commands.size());
	}

	/**
	 * Returns command object with the given name.
	 * 
	 * @param commandName
	 *            Name of the command.
	 * @return Command object.
	 */
	public static Command get(String commandName) {
		if (commandName == null || !commands.containsKey(commandName)) {
			LOG.trace("Command not found, name --> " + commandName);
			return commands.get("noCommand"); 
		}
		
		return commands.get(commandName);
	}
	
}