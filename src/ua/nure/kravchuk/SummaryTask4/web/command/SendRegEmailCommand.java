package ua.nure.kravchuk.SummaryTask4.web.command;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.exception.AppException;
import ua.nure.kravchuk.SummaryTask4.repository.entity.User;
import ua.nure.kravchuk.SummaryTask4.sevices.UserService;
import ua.nure.kravchuk.SummaryTask4.sevices.web.SendEmail;
import ua.nure.kravchuk.SummaryTask4.sevices.web.Validation;
import ua.nure.kravchuk.SummaryTask4.util.Constants;

/**
 * Send letter to user's email with registration url command.
 * 
 * @author P.Kravchuk
 *
 */
public class SendRegEmailCommand extends Command {

	private static final long serialVersionUID = -8346743591136040692L;

	private static final Logger LOG = Logger.getLogger(SendRegEmailCommand.class);

	@Override
	public String logic(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		String forward = Constants.Path.ERROR;
		UserService manager = new UserService();
		Validation validation = new Validation();

		String login = request.getParameter("login");
		LOG.trace("Request parameter: login --> " + login);
		validation.checkParameterLogin(login, "login");

		User user1 = manager.findUserByLogin(login);
		if (user1 != null) {
			LOG.error("User with such login is already exist");
			throw new AppException("We already have user with such login.");
		}

		String email = request.getParameter("email");
		LOG.trace("Request parameter: email --> " + email);
		validation.checkParameterEmail(email, "email");

		User user2 = manager.findUserByEmail(email);
		if (user2 != null) {
			LOG.error("User with such email is already exist");
			throw new AppException("We already have user with such email.");
		}

		String password1 = request.getParameter("password1");
		LOG.trace("Request parameter: password1 --> " + password1);
		String password2 = request.getParameter("password2");
		LOG.trace("Request parameter: password2 --> " + password2);
		String name = request.getParameter("name");
		LOG.trace("Request parameter: name --> " + name);
		String surname = request.getParameter("surname");
		LOG.trace("Request parameter: surname --> " + surname);
		String phone = request.getParameter("phone");
		LOG.trace("Request parameter: phone --> " + phone);
		String country = request.getParameter("country");
		LOG.trace("Request parameter: country --> " + country);
		String city = request.getParameter("city");
		LOG.trace("Request parameter: city --> " + city);

		validation.validateAllParametersForNewUser(password1, password2, name, surname, phone, country, city);
		LOG.debug("All parameters have been validated successfully");

		SendEmail sendEmail = new SendEmail();
		String url = createRegUrl(login, email, password1, name, surname, country, city, phone);
		sendEmail.sendLetterToEmail(email, "Registration in MyLibrary", "Registration in MyLibrary",
				createMessage(url));

		forward = Constants.Path.BEGIN;

		return forward;

	}

	/**
	 * Method for creating link for new user registration
	 */
	public String createRegUrl(String login, String email, String password, String name, String surname, String country,
			String city, String phone) {
		StringBuilder url = new StringBuilder();

		url.append("http://localhost:8080/SummaryTask4/controller?command=registration&");
		url.append("login=").append(login).append("&");
		url.append("password=").append(password).append("&");
		url.append("email=").append(email).append("&");
		url.append("name=").append(name);

		if (surname != null) {
			url.append("&");
			url.append("surname=").append(surname);
		}
		if (country != null) {
			url.append("&");
			url.append("country=").append(country);
		}
		if (city != null) {
			url.append("&");
			url.append("city=").append(city);
		}
		if (phone != null) {
			url.append("&");
			url.append("phone=").append(phone);
		}

		return url.toString();
	}

	/**
	 * Method for creating message for new user registration
	 */
	public String createMessage(String url) {
		StringBuilder message = new StringBuilder();

		message.append("Dear user, please follow this link to finish your registration.");
		message.append("</p>").append("<p/>").append("<p>");
		message.append(url);
		message.append("</p>").append("<p/>").append("<p>");
		message.append("My Library - Best for you.");

		return message.toString();
	}

	

}
