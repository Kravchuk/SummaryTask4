package ua.nure.kravchuk.SummaryTask4.web.command;

import java.io.IOException;
import java.io.Serializable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.exception.AppException;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Status;
import ua.nure.kravchuk.SummaryTask4.sevices.OrderService;
import ua.nure.kravchuk.SummaryTask4.util.Constants;

/**
 * Main interface for the Command pattern implementation.
 * 
 * @author P.Kravchuk
 * 
 */
public abstract class Command implements Serializable {

	private static final long serialVersionUID = -698143994525482600L;
	
	private static final Logger LOG = Logger.getLogger(Command.class);

	/**
	 * Execution method for command.
	 * 
	 * @return <tt>forward</tt> address to go once the command is executed.
	 */
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		LOG.debug("Command starts");
		
		String forward = logic(request, response);
		
		LOG.debug("Command finished");
		return forward;
	}

	/**
	 * Logic method for command.
	 * 
	 * @return <tt>forward</tt> address to go once the command is executed.
	 */
	public abstract String logic(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException;

	@Override
	public final String toString() {
		return getClass().getSimpleName();
	}
	
	protected String returnBookCommand(Logger LOG, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException{
		String forward = Constants.Path.ERROR;

		OrderService manager = new OrderService();

		int orderId = Integer.parseInt(request.getParameter("orderId"));
		LOG.trace("Request parameter: orderId --> " + orderId);
		
		boolean result = manager.updateOrderStatus(orderId, Status.RETURNED);

		if (!result) {
			LOG.debug("Cannot return this book, DB error");
			throw new AppException("Cannot return this book, DB error");
		} else {
			LOG.trace("Order was updated: order with id --> " + orderId + " :returned");
		}

		forward = Constants.Path.SEND_REDIRECT;

		return forward;
	}
	
	
	
	
	
}