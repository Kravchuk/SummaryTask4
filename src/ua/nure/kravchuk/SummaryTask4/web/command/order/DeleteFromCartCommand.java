package ua.nure.kravchuk.SummaryTask4.web.command.order;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.exception.AppException;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Order;
import ua.nure.kravchuk.SummaryTask4.util.Constants;
import ua.nure.kravchuk.SummaryTask4.web.command.Command;

/**
 * Delete order from cart command.
 * 
 * @author P.Kravchuk
 * 
 */
public class DeleteFromCartCommand extends Command {

	private static final long serialVersionUID = -4082703343854353954L;

	private static final Logger LOG = Logger.getLogger(DeleteFromCartCommand.class);

	@Override
	public String logic(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		HttpSession session = request.getSession();
		String forward = Constants.Path.ERROR;
		
		String deleteByBookId = request.getParameter("delete");
		int bookId = Integer.parseInt(deleteByBookId);
		if (deleteByBookId != null && bookId > -1) {

			@SuppressWarnings("unchecked")
			List<Order> cart = (List<Order>) session.getAttribute("cart");
			LOG.trace("Getting attribute cart from session, cart.size -->" + cart.size());
			
			for (int i = 0; i < cart.size(); i++) {
				if(cart.get(i).getBookId() == bookId){
					cart.remove(i);
					LOG.trace("Order was removed from cart, bookId -->" + bookId);
					break;
				}
			}

			session.removeAttribute("cart");
			session.setAttribute("cart", cart);
			LOG.debug("Session parameter updating: cart");

			forward = Constants.Path.CART;
		}else{
			LOG.trace("Wrong parameter for deleting order");
			throw new AppException("Wrong parameter for deleting order");
		}

		return forward;
	}

}
