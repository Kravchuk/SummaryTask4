package ua.nure.kravchuk.SummaryTask4.web.command.order;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.exception.AppException;
import ua.nure.kravchuk.SummaryTask4.web.command.Command;

/**
 * Return book command.
 * 
 * @author P.Kravchuk
 * 
 */
public class ReturnBookCommand extends Command {

	private static final long serialVersionUID = 3801665881459506418L;

	private static final Logger LOG = Logger.getLogger(ReturnBookCommand.class);

	@Override
	public String logic(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
	
		//logic for paying penalty
		return returnBookCommand(LOG, request, response);
	}

}
