package ua.nure.kravchuk.SummaryTask4.web.command.order;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.exception.AppException;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Book;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Order;
import ua.nure.kravchuk.SummaryTask4.sevices.CatalogService;
import ua.nure.kravchuk.SummaryTask4.sevices.OrderService;
import ua.nure.kravchuk.SummaryTask4.util.Constants;
import ua.nure.kravchuk.SummaryTask4.web.command.Command;

/**
 * Make order command.
 * 
 * @author P.Kravchuk
 * 
 */
public class MakeOrderCommand extends Command {

	private static final long serialVersionUID = -3713383132981708852L;

	private static final Logger LOG = Logger.getLogger(MakeOrderCommand.class);

	@Override
	public String logic(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		HttpSession session = request.getSession();
		String forward = Constants.Path.ERROR;

		String begin = request.getParameter("begin");
		
		if (begin != null) {
			forward = Constants.Path.CART;
			LOG.debug("Request parameter: begin != null --> go to cart page");
			return forward;
		}
		
		OrderService manager = new OrderService();

		@SuppressWarnings("unchecked")
		List<Order> cart = (List<Order>) session.getAttribute("cart");

		if (cart == null || cart.size() == 0) {
			LOG.debug("Cannot open cart, empty cart");
			throw new AppException("Cannot open cart, empty cart");
		}

		
		for (int i = 0; i < cart.size(); i++) {
			Order order = cart.get(i);
			boolean result = manager.makeOrder(order);

			if (result) {
				CatalogService managerCat = new CatalogService();
				Book book = managerCat.findBookById(order.getBookId());
				if (book != null) {
					book.setAvailable(book.getAvailable() - 1);
					result = managerCat.updateBook(order.getBookId(), book);
					LOG.trace("Book was found and updated: book --> " + book);
				}
			} else {
				LOG.debug("Cannot make this order, DB error");
				throw new AppException("Cannot make this order, DB error");
			}
		}

		session.removeAttribute("cart");
		LOG.debug("Removing attribute cart from session");
		
		forward = Constants.Path.SEND_REDIRECT;

		return forward;
	}

}
