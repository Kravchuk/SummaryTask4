package ua.nure.kravchuk.SummaryTask4.web.command.order;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.exception.AppException;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Status;
import ua.nure.kravchuk.SummaryTask4.sevices.OrderService;
import ua.nure.kravchuk.SummaryTask4.util.Constants;
import ua.nure.kravchuk.SummaryTask4.web.command.Command;

/**
 * Give book command.
 * 
 * @author P.Kravchuk
 * 
 */
public class GiveBookCommand extends Command {

	private static final long serialVersionUID = -7157987336037741542L;

	private static final Logger LOG = Logger.getLogger(GiveBookCommand.class);

	@Override
	public String logic(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		String forward = Constants.Path.ERROR;

		int orderId = Integer.parseInt(request.getParameter("orderId"));
		LOG.trace("Request parameter: orderId --> " + orderId);
		
		OrderService manager = new OrderService();
		boolean result = manager.updateOrderStatus(orderId, Status.GIVEN_TO_USER);

		if (!result) {
			LOG.debug("Cannot give book, DB error");
			throw new AppException("Cannot give book, DB error");
		} else {
			LOG.trace("Book is given to user successfully, orderId --> " + orderId);
		}

		forward = Constants.Path.SEND_REDIRECT;

		return forward;
	}

}
