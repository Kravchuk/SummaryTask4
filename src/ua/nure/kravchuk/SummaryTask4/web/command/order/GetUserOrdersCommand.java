package ua.nure.kravchuk.SummaryTask4.web.command.order;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.exception.AppException;
import ua.nure.kravchuk.SummaryTask4.repository.entity.User;
import ua.nure.kravchuk.SummaryTask4.repository.entity.bean.OrderBookBean;
import ua.nure.kravchuk.SummaryTask4.sevices.OrderService;
import ua.nure.kravchuk.SummaryTask4.util.Constants;
import ua.nure.kravchuk.SummaryTask4.web.command.Command;

/**
 * Get user orders command.
 * 
 * @author P.Kravchuk
 * 
 */
public class GetUserOrdersCommand extends Command {

	private static final long serialVersionUID = -3713383132981708852L;

	private static final Logger LOG = Logger.getLogger(GetUserOrdersCommand.class);

	@Override
	public String logic(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		
		String forward = Constants.Path.ERROR;

		OrderService manager = new OrderService();

		List<OrderBookBean> orders = manager.findBeanOrdersByUserId(user.getUserId());

		if (orders == null) {
			LOG.debug("User has no orders");
		} else {
			LOG.trace("Orders returned: orders count --> " + orders.size());
		}

		forward = Constants.Path.USER_ORDERS;
		request.setAttribute("orders", orders);
		LOG.trace("Request setting attribute: orders , size -->" + orders.size());
		
		return forward;
	}

}
