package ua.nure.kravchuk.SummaryTask4.web.command.order;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.exception.AppException;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Order;
import ua.nure.kravchuk.SummaryTask4.sevices.OrderService;
import ua.nure.kravchuk.SummaryTask4.util.Constants;
import ua.nure.kravchuk.SummaryTask4.web.command.Command;

/**
 * Get unconfirmed orders command.
 * 
 * @author P.Kravchuk
 * 
 */
public class GetUnconfirmedOrdersCommand extends Command {

	private static final long serialVersionUID = -637850314574440428L;

	private static final Logger LOG = Logger.getLogger(GetUnconfirmedOrdersCommand.class);

	@Override
	public String logic(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		String forward = Constants.Path.ERROR;

		OrderService manager = new OrderService();

		List<Order> orders = manager.getUnconfirmedOrders();

		if (orders == null) {
			LOG.debug("We have not any unconfirmed orders");
		} else {
			LOG.trace("Orders returned: orders count --> " + orders.size());
		}

		forward = Constants.Path.UNCONFIRMED_ORDERS;
		request.setAttribute("orders", orders);
		LOG.trace("Request setting attribute: orders , size -->" + orders.size());
		
		return forward;
	}

}
