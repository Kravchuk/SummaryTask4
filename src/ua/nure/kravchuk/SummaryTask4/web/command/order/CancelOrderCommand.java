package ua.nure.kravchuk.SummaryTask4.web.command.order;

import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.exception.AppException;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Order;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Status;
import ua.nure.kravchuk.SummaryTask4.sevices.OrderService;
import ua.nure.kravchuk.SummaryTask4.util.Constants;
import ua.nure.kravchuk.SummaryTask4.web.command.Command;

/**
 * Cancel order command.
 * 
 * @author P.Kravchuk
 * 
 */
public class CancelOrderCommand extends Command {

	private static final long serialVersionUID = 8995612060354926895L;
	
	private static final Logger LOG = Logger.getLogger(CancelOrderCommand.class);

	@Override
	public String logic(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		String forward = Constants.Path.ERROR;

		String id = request.getParameter("orderId");
		if (id == null) {
			LOG.debug("Wrong parameter orderId");
			throw new AppException("Wrong parameter orderId");
		}else{
			LOG.trace("Request parameter: orderId --> " + id);
		}
		
		int orderId = Integer.parseInt(id);
		
		OrderService manager = new OrderService();

		Order order = manager.findOrderById(orderId);

		if (order != null && order.getStatus().equals(Status.NEW_REQUEST)) {
				boolean result = manager.deleteOrderById(orderId);
				
				if(result == false){
					LOG.debug("Cannot delete this order, DB error");
					throw new AppException("Cannot delete this order, DB error");
				}else{
					LOG.debug("Order was deleted successfully, order --> " + order.toString());
				}
		} else {
			LOG.debug("Cannot find this order, DB error or order.status != NEW_REQUEST");
			throw new AppException("Cannot find this order, DB error or order.status != NEW_REQUEST");
		}
		
		
		forward = Constants.Path.SEND_REDIRECT;

		return forward;
	}

}
