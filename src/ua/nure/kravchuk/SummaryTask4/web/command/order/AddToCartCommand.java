package ua.nure.kravchuk.SummaryTask4.web.command.order;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.exception.AppException;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Order;
import ua.nure.kravchuk.SummaryTask4.repository.entity.User;
import ua.nure.kravchuk.SummaryTask4.util.Constants;
import ua.nure.kravchuk.SummaryTask4.web.command.Command;

/**
 * Add order to cart command.
 * 
 * @author P.Kravchuk
 * 
 */
public class AddToCartCommand extends Command {

	private static final long serialVersionUID = 713346841585723677L;

	private static final Logger LOG = Logger.getLogger(AddToCartCommand.class);

	@Override
	public String logic(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		if(user.isBlocked() == true){
			LOG.debug("User, who trying to make order, is blocked");
			throw new AppException("You are blocked");
		}
		

		String forward = Constants.Path.ERROR;

		boolean isInReadingRoom = Boolean.parseBoolean(request.getParameter("isInReadingRoom"));
		LOG.trace("Request parameter: isInReadingRoom --> " + isInReadingRoom);

		int bookId = Integer.parseInt(request.getParameter("bookId"));
		LOG.trace("Request parameter: bookId --> " + bookId);

		Order newOrder = Order.createOrder(isInReadingRoom, user.getUserId(), bookId);

		@SuppressWarnings("unchecked")
		List<Order> cart = (List<Order>) session.getAttribute("cart");

		if (cart == null) {
			cart = new ArrayList<>();
		}

		ckeckCart(cart, bookId);
		LOG.trace("Order add to cart: order --> " + newOrder);
		
		cart.add(newOrder);

		session.setAttribute("cart", cart);
		LOG.debug("Session parameter adding: cart");
		
		forward = Constants.Path.SEND_REDIRECT;

		return forward;
	}

	private void ckeckCart(List<Order> cart, int bookId) {
		if (cart.size() > Constants.AppConsts.MAX_SIZE_OF_CART) {
			LOG.debug("User has already have maximum count of books(" + Constants.AppConsts.MAX_SIZE_OF_CART +") in cart");
			throw new AppException("You have already have maximum count of books(" + Constants.AppConsts.MAX_SIZE_OF_CART +") in cart");
		}

		for (int i = 0; i < cart.size(); i++) {
				int bookId2 = cart.get(i).getBookId();
				if (bookId == bookId2) {
					LOG.debug("User has already have thr same book in cart");
					throw new AppException("You have already have the same book in cart");
				
				}
			}

	}		

		
}
