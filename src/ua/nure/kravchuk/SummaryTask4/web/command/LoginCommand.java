package ua.nure.kravchuk.SummaryTask4.web.command;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.exception.AppException;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Role;
import ua.nure.kravchuk.SummaryTask4.repository.entity.User;
import ua.nure.kravchuk.SummaryTask4.sevices.UserService;
import ua.nure.kravchuk.SummaryTask4.sevices.web.CaptchaVerifier;
import ua.nure.kravchuk.SummaryTask4.util.Constants;



/**
 * Login command.
 * 
 * @author P.Kravchuk
 * 
 */
public class LoginCommand extends Command {

	private static final long serialVersionUID = -3071536593627692473L;

	private static final Logger LOG = Logger.getLogger(LoginCommand.class);

	@Override
	public String logic(HttpServletRequest request, HttpServletResponse response) 
			throws IOException, ServletException, AppException {
		
		HttpSession session = request.getSession();
		String forward = Constants.Path.ERROR;
		CaptchaVerifier verifier = new CaptchaVerifier();
		
		verifier.offlineCaptchaCheck(session, request, response);

		
		String loginOrEmail = request.getParameter("login");
		LOG.trace("Request parameter: login --> " + loginOrEmail);

		String password = request.getParameter("password");
		if (loginOrEmail == null || password == null || loginOrEmail.isEmpty() || password.isEmpty()) {
			LOG.trace(Constants.AppErrors.EMPTY_LOGIN_PASSWORD);
			throw new AppException(Constants.AppErrors.EMPTY_LOGIN_PASSWORD);
		}

		UserService manager = new UserService();
		User user = manager.findUserByLogin(loginOrEmail);
		LOG.trace("Found in DB by login: user --> " + user);
		
		if(user == null){
			user = manager.findUserByEmail(loginOrEmail);
			LOG.trace("Found in DB by email: user --> " + user);
		}

		
		if (user == null || !password.equals(user.getPassword())) {
			LOG.trace(Constants.AppErrors.FIND_USER);
			throw new AppException(Constants.AppErrors.FIND_USER);
		}

		Role userRole = Role.getRole(user);
		LOG.trace("userRole --> " + userRole);

		session.setAttribute("user", user);
		LOG.trace("Set the session attribute: user --> " + user);

		session.setAttribute("userRole", userRole);
		LOG.trace("Set the session attribute: userRole --> " + userRole);

		LOG.info("User " + user + " logged as " + userRole.toString().toLowerCase());
		
		forward = Constants.Path.SEND_REDIRECT;

		return forward;
	}

	


}