package ua.nure.kravchuk.SummaryTask4.web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.util.Constants;

/**
 * No command.
 * 
 * @author P.Kravchuk
 * 
 */
public class NoCommand extends Command {

	private static final long serialVersionUID = 2607009520345114075L;


	private static final Logger LOG = Logger.getLogger(NoCommand.class);

	@Override
	public String logic(HttpServletRequest request, HttpServletResponse response) {
		
		String errorMessage = "No such command";
		request.setAttribute("errorMessage", errorMessage);
		LOG.error("Set the request attribute: errorMessage --> " + errorMessage);

		return Constants.Path.ERROR;
	}

}