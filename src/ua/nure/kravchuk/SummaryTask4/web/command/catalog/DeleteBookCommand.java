package ua.nure.kravchuk.SummaryTask4.web.command.catalog;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.exception.AppException;
import ua.nure.kravchuk.SummaryTask4.sevices.CatalogService;
import ua.nure.kravchuk.SummaryTask4.util.Constants;
import ua.nure.kravchuk.SummaryTask4.web.command.Command;

/**
 * Delete book command.
 * 
 * @author P.Kravchuk
 * 
 */
public class DeleteBookCommand extends Command {

	private static final long serialVersionUID = -5465279760772478881L;

	private static final Logger LOG = Logger.getLogger(DeleteBookCommand.class);

	@Override
	public String logic(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		int bookId = Integer.parseInt(request.getParameter("bookId"));
		LOG.trace("Request parameter: bookId --> " + bookId);
		
		String forward = Constants.Path.ERROR;
		CatalogService manager = new CatalogService();
		boolean result = manager.deleteBookById(bookId);

		if (!result) {
			LOG.debug("Cannot delete book");
			throw new AppException("Cannot delete book, DB error.");
		} else {
			LOG.trace("Book deleted successfully, bookId --> " + bookId);
		}

		forward = Constants.Path.SEND_REDIRECT;

		return forward;
	}

}
