package ua.nure.kravchuk.SummaryTask4.web.command.catalog;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.exception.AppException;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Book;
import ua.nure.kravchuk.SummaryTask4.sevices.CatalogService;
import ua.nure.kravchuk.SummaryTask4.sevices.web.Validation;
import ua.nure.kravchuk.SummaryTask4.util.Constants;
import ua.nure.kravchuk.SummaryTask4.web.command.Command;

/**
 * Add new book command.
 * 
 * @author P.Kravchuk
 * 
 */
public class AddNewBookCommand extends Command {

	private static final long serialVersionUID = 4946599069759337368L;

	private static final Logger LOG = Logger.getLogger(AddNewBookCommand.class);

	@Override
	public String logic(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		
		String forward = Constants.Path.ERROR;
		String begin = request.getParameter("begin");
		Validation validation = new Validation();
		
		if (begin != null) {
			forward = Constants.Path.ADD_BOOK;
			LOG.debug("Request parameter: begin != null --> go to add_book page");
			return forward;
		}

		String name = request.getParameter("name");
		LOG.trace("Request parameter: name --> " + name);
		
		String author = request.getParameter("author");
		LOG.trace("Request parameter: author --> " + author);
		
		String publisher = request.getParameter("publisher");
		LOG.trace("Request parameter: publisher --> " + publisher);
		
		String genre = request.getParameter("genre");
		LOG.trace("Request parameter: genre --> " + genre);
		
		int totalAmount = Integer.parseInt(request.getParameter("totalAmount"));
		LOG.trace("Request parameter: totalAmount --> " + totalAmount);
		
		int numberOfDaysForUse = Integer.parseInt(request.getParameter("numberOfDaysForUse"));
		LOG.trace("Request parameter: numberOfDaysForUse --> " + numberOfDaysForUse);
		
		double penaltyPayment = Double.parseDouble(request.getParameter("penaltyPayment"));
		LOG.trace("Request parameter: penaltyPayment --> " + penaltyPayment);
		
		String dateStr = request.getParameter("publicationDate");
		LOG.trace("Request parameter: publicationDate --> " + dateStr);

		validation.validateAllParametersForNewBook(name, author, publisher, genre, dateStr.split("-"), totalAmount, numberOfDaysForUse, penaltyPayment);
	
		Date date = validation.stringToDate(dateStr);
		
		LOG.debug("All parameters have been validated successfully");
		
		Book book = Book.createBook(name, author, publisher, genre, date, totalAmount, numberOfDaysForUse,
				penaltyPayment);

		CatalogService manager = new CatalogService();
		boolean result = manager.insertBook(book);

		if (!result) {
			LOG.debug("Cannot add new book");
			throw new AppException("Cannot add new book");
		} else {
			LOG.trace("Book added successfully, book --> " + book);
		}

		forward = Constants.Path.SEND_REDIRECT;

		return forward;
	}

	
}
