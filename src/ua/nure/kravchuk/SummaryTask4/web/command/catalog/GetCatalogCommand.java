package ua.nure.kravchuk.SummaryTask4.web.command.catalog;

import java.io.IOException;
import java.util.List;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.exception.AppException;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Book;
import ua.nure.kravchuk.SummaryTask4.sevices.CatalogService;
import ua.nure.kravchuk.SummaryTask4.util.Constants;
import ua.nure.kravchuk.SummaryTask4.web.command.Command;

/**
 * Get catalog command.
 * 
 * @author P.Kravchuk
 * 
 */
public class GetCatalogCommand extends Command {

	private static final long serialVersionUID = -3713383132981708852L;

	private static final Logger LOG = Logger.getLogger(GetCatalogCommand.class);

	@Override
	public String logic(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		String forward;
		CatalogService manager = new CatalogService();
		
		List<Book> books = manager.getCatalog("naturalOrder");

		if (books == null) {
			LOG.debug("Library has no books or DB error");
			throw new AppException("Library has no books or DB error");
		} else {
			LOG.trace("Books returned: books count --> " + books.size());
		}

		forward = Constants.Path.CATALOG;
		
		request.setAttribute("books", books);
		LOG.trace("Request setting attribute: books , size -->" + books.size());
		
		return forward;
	}

}
