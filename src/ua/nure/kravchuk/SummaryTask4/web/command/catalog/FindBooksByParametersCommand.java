package ua.nure.kravchuk.SummaryTask4.web.command.catalog;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.exception.AppException;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Book;
import ua.nure.kravchuk.SummaryTask4.sevices.CatalogService;
import ua.nure.kravchuk.SummaryTask4.util.Constants;
import ua.nure.kravchuk.SummaryTask4.web.command.Command;

/**
 * Find book by name, by author, by genre, by publisher, by publication date,
 * order books by different parameters and reverse command.
 * 
 * @author P.Kravchuk
 * 
 */
public class FindBooksByParametersCommand extends Command {

	private static final long serialVersionUID = 4189334759938668945L;

	private static final Logger LOG = Logger.getLogger(FindBooksByParametersCommand.class);

	@Override
	public String logic(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		HttpSession session = request.getSession();
		String forward;

		CatalogService manager = new CatalogService();

		if (request.getParameter("clear") != null) {
			LOG.trace("Request get parameter: clear --> " + "clear");
			List<Book> books = manager.getCatalog("naturalOrder");

			if (books.size() > 0) {
				LOG.trace("Books returned: books count --> " + books.size());
			} else {
				LOG.debug("You got no books");
				throw new AppException("You got no books");
			}

			forward = Constants.Path.CATALOG;
			deleteSessionAttributes(session);

			request.setAttribute("books", books);
			LOG.trace("Request setting attribute: books , size -->" + books.size());

			return forward;
		}

		String name = checkParameter(request.getParameter("name"), "name");
		String author = checkParameter(request.getParameter("author"), "author");
		String publisher = checkParameter(request.getParameter("publisher"), "publisher");
		String genre = checkParameter(request.getParameter("genre"), "genre");
		String date = checkParameter(request.getParameter("publicationDate"), "publicationDate");
		String orderBy = checkOrderBy(request.getParameter("orderBy"));
		boolean reverse = false;
		if (request.getParameter("reverse") != null) {
			reverse = Boolean.parseBoolean(request.getParameter("reverse"));
		}

		if (name == null && author == null && publisher == null && genre == null && date == null && orderBy == null) {
			LOG.error("All parameters for finding books are null");
			throw new AppException("All finding parameters are null");
		}

		List<Book> books = manager.findBookByParameters(name, author, publisher, genre, date, orderBy, reverse);

		if (books.size() > 0) {
			LOG.trace("Books returned: books count --> " + books.size());
		} else {
			LOG.debug("You got no books");
			throw new AppException("You got no books");
		}

		forward = Constants.Path.CATALOG;
		deleteSessionAttributes(session);

		putParametersToSession(session, name, author, publisher, genre, date, orderBy, reverse);

		request.setAttribute("books", books);
		LOG.trace("Request setting attribute: books , size -->" + books.size());

		return forward;
	}

	private String checkParameter(String str, String message) {
		if (str == null || str.length() < 1) {
			return null;
		} else {
			LOG.trace("Request parameter: " + message + "--> " + str);
			return str;
		}

	}

	private void deleteSessionAttributes(HttpSession session) {
		if (session.getAttribute("name") != null) {
			session.removeAttribute("name");
			LOG.debug("Session deleting attribute: name");
		}
		if (session.getAttribute("author") != null) {
			session.removeAttribute("author");
			LOG.debug("Session deleting attribute: author");
		}
		if (session.getAttribute("publisher") != null) {
			session.removeAttribute("publisher");
			LOG.debug("Session deleting attribute: publisher");
		}
		if (session.getAttribute("genre") != null) {
			session.removeAttribute("genre");
			LOG.debug("Session deleting attribute: genre");
		}
		if (session.getAttribute("publicationDate") != null) {
			session.removeAttribute("publicationDate");
			LOG.debug("Session deleting attribute: publicationDate");
		}
		if (session.getAttribute("orderBy") != null) {
			session.removeAttribute("orderBy");
			LOG.debug("Session deleting attribute: orderBy");
		}
		if (session.getAttribute("reverse") != null) {
			session.removeAttribute("reverse");
			LOG.debug("Session deleting attribute: reverse");
		}
	}

	private String checkOrderBy(String orderBy) {
		List<String> orderByParameters = new ArrayList<String>();
		orderByParameters.add("name");
		orderByParameters.add("author");
		orderByParameters.add("publisher");
		orderByParameters.add("publicationDate");
		orderByParameters.add("popularity");
		orderByParameters.add("naturalOrder");

		if (orderBy != null && !orderByParameters.contains(orderBy)) {
			LOG.error("Wrong orderBy parameter");
			throw new AppException("Wrong orderBy parameter");
		}

		return orderBy;
	}

	private void putParametersToSession(HttpSession session, String name, String author, String publisher, String genre,
			String date, String orderBy, boolean reverse) {
		if (name != null) {
			session.setAttribute("name", name);
			LOG.trace("Session setting attribute: name -->" + name);
		}
		if (author != null) {
			session.setAttribute("author", author);
			LOG.trace("Session setting attribute: author -->" + author);
		}
		if (publisher != null) {
			session.setAttribute("publisher", publisher);
			LOG.trace("Session setting attribute: publisher -->" + publisher);
		}
		if (genre != null) {
			session.setAttribute("genre", genre);
			LOG.trace("Session setting attribute: genre -->" + genre);
		}
		if (date != null) {
			session.setAttribute("publicationDate", date);
			LOG.trace("Session setting attribute: publicationDate -->" + date);
		}
		if (orderBy != null) {
			session.setAttribute("orderBy", orderBy);
			LOG.trace("Session setting attribute: orderBy -->" + orderBy);
		}
		if (reverse != false) {
			session.setAttribute("reverse", reverse);
			LOG.trace("Session setting attribute: reverse -->" + reverse);
		}
	}

}
