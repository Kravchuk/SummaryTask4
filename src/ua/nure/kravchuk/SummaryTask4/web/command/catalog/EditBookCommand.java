package ua.nure.kravchuk.SummaryTask4.web.command.catalog;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.exception.AppException;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Book;
import ua.nure.kravchuk.SummaryTask4.sevices.CatalogService;
import ua.nure.kravchuk.SummaryTask4.sevices.web.Validation;
import ua.nure.kravchuk.SummaryTask4.util.Constants;
import ua.nure.kravchuk.SummaryTask4.web.command.Command;

/**
 * Edit book command.
 * 
 * @author P.Kravchuk
 * 
 */
public class EditBookCommand extends Command {

	private static final long serialVersionUID = 6015731929860639835L;

	private static final Logger LOG = Logger.getLogger(FindBookCommand.class);

	@Override
	public String logic(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		String forward = Constants.Path.ERROR;
		Validation validation = new Validation();
		
		
		int bookId = Integer.parseInt(request.getParameter("bookId"));
		LOG.trace("Request parameter: bookId --> " + bookId);
		
		String name = request.getParameter("name");
		LOG.trace("Request parameter: name --> " + name);
		
		String author = request.getParameter("author");
		LOG.trace("Request parameter: author --> " + author);
		
		String publisher = request.getParameter("publisher");
		LOG.trace("Request parameter: publisher --> " + publisher);
		
		String genre = request.getParameter("genre");
		LOG.trace("Request parameter: genre --> " + genre);
		
		int totalAmount = Integer.parseInt(request.getParameter("totalAmount"));
		LOG.trace("Request parameter: totalAmount --> " + totalAmount);
		
		int numberOfDaysForUse = Integer.parseInt(request.getParameter("numberOfDaysForUse"));
		LOG.trace("Request parameter: numberOfDaysForUse --> " + numberOfDaysForUse);
		
		double penaltyPayment = Double.parseDouble(request.getParameter("penaltyPayment"));
		LOG.trace("Request parameter: penaltyPayment --> " + penaltyPayment);
		
		String dateStr = request.getParameter("publicationDate");
		LOG.trace("Request parameter: publicationDate --> " + dateStr);
		
		int available = Integer.parseInt(request.getParameter("available"));
		LOG.trace("Request parameter: available --> " + available);
		
		int numberOfReadings = Integer.parseInt(request.getParameter("numberOfReadings"));
		LOG.trace("Request parameter: numberOfReadings --> " + numberOfReadings);

		
		validation.validateAllParametersForNewBook(name, author, publisher, genre, dateStr.split("-"), totalAmount,
				numberOfDaysForUse, penaltyPayment);

		validation.checkParameterInt(numberOfReadings, "number of readings");
		validation.checkParameterAvailable(available, totalAmount, "available");
		Date date = validation.stringToDate(dateStr);
		LOG.debug("All parameters have been validated successfully");

		Book book = Book.createBook(name, author, publisher, genre, date, totalAmount, numberOfDaysForUse,
				penaltyPayment);
		book.setAvailable(available);
		book.setNumberOfReadings(numberOfReadings);

		CatalogService manager = new CatalogService();
		boolean result = manager.updateBook(bookId, book);

		if (!result) {
			LOG.debug("Cannot update book");
			throw new AppException("Cannot update book, DB error.");
		} else {
			LOG.trace("Book updated successfully, bookId --> " + bookId);
		}

		forward = Constants.Path.SEND_REDIRECT;

		return forward;
	}


}
