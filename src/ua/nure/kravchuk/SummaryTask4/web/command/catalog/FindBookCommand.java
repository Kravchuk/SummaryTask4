package ua.nure.kravchuk.SummaryTask4.web.command.catalog;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.exception.AppException;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Book;
import ua.nure.kravchuk.SummaryTask4.sevices.CatalogService;
import ua.nure.kravchuk.SummaryTask4.util.Constants;
import ua.nure.kravchuk.SummaryTask4.web.command.Command;

/**
 * Find book command.
 * 
 * @author P.Kravchuk
 * 
 */
public class FindBookCommand extends Command {

	private static final long serialVersionUID = 6015731929860639835L;

	private static final Logger LOG = Logger.getLogger(FindBookCommand.class);

	@Override
	public String logic(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		int bookId = Integer.parseInt(request.getParameter("bookId"));
		LOG.trace("Request parameter: bookId --> " + bookId);
		
		String forward = Constants.Path.ERROR;

		CatalogService manager = new CatalogService();
		Book book = manager.findBookById(bookId);

		if (book == null) {
			LOG.debug("Cannot find book with such id");
			throw new AppException("Cannot find book with such id, DB error");
		} else {
			request.setAttribute("book", book);
			LOG.trace("Book returned, bookId --> " + bookId);
		}

		forward = Constants.Path.BOOK_PROFILE;

		return forward;
	}

}
