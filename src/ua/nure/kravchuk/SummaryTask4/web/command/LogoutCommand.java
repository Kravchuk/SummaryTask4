package ua.nure.kravchuk.SummaryTask4.web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.util.Constants;


/**
 * Logout command.
 * 
 * @author P.Kravchuk
 * 
 */
public class LogoutCommand extends Command {

	private static final long serialVersionUID = -6305800113934432870L;

	private static final Logger LOG = Logger.getLogger(LogoutCommand.class);

	@Override
	public String logic(HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession session = request.getSession(false);
		if (session != null) {
			session.invalidate();
			LOG.debug("Invalidation success");
		}
		
		return Constants.Path.SEND_REDIRECT;
	}

}