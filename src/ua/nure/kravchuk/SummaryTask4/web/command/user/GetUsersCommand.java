package ua.nure.kravchuk.SummaryTask4.web.command.user;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.exception.AppException;
import ua.nure.kravchuk.SummaryTask4.repository.entity.User;
import ua.nure.kravchuk.SummaryTask4.repository.entity.bean.UserBookBean;
import ua.nure.kravchuk.SummaryTask4.sevices.UserService;
import ua.nure.kravchuk.SummaryTask4.util.Constants;
import ua.nure.kravchuk.SummaryTask4.web.command.Command;

/**
 * Get users and librarians command.
 * 
 * @author P.Kravchuk
 * 
 */
public class GetUsersCommand extends Command {

	private static final long serialVersionUID = 242667366736071919L;

	private static final Logger LOG = Logger.getLogger(GetUsersCommand.class);

	@Override
	public String logic(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		String isLibrarians = request.getParameter("isLibrarians");
		LOG.trace("Request parameter: isLibrarians --> " + isLibrarians);
		
		String forward = Constants.Path.ERROR;

		UserService manager = new UserService();

		//List<User> users = manager.findUsers(isLibrarians != null);
		List<UserBookBean> users = manager.findUsersTest();

		if (users == null) {
			LOG.debug("Cannot obatin users, empty list or DB error");
			throw new AppException("Cannot obatin users, empty list or DB error");
		} else {
			LOG.trace("Users returned: users count --> " + users.size());
		}

		
		if (isLibrarians == null) {
			request.setAttribute("users", users);
		} else {
			request.setAttribute("librarians", users);
		}

		LOG.debug("Request setting attribute: users");
		forward = Constants.Path.USERS;
		
		return forward;
	}

}
