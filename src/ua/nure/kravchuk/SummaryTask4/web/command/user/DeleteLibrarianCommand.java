package ua.nure.kravchuk.SummaryTask4.web.command.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.exception.AppException;
import ua.nure.kravchuk.SummaryTask4.sevices.UserService;
import ua.nure.kravchuk.SummaryTask4.util.Constants;
import ua.nure.kravchuk.SummaryTask4.web.command.Command;

/**
 * Delete librarian command.
 * 
 * @author P.Kravchuk
 * 
 */
public class DeleteLibrarianCommand extends Command {

	private static final long serialVersionUID = 41360418767705847L;

	private static final Logger LOG = Logger.getLogger(DeleteLibrarianCommand.class);

	@Override
	public String logic(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		String forward = Constants.Path.ERROR;

		int librarianId = Integer.parseInt(request.getParameter("librarianId"));
		LOG.trace("Request parameter: userId --> " + librarianId);
		
		UserService manager = new UserService();
		boolean result = manager.deleteUser(librarianId);

		if (!result) {
			LOG.debug("Cannot delete librarian, DB error");
			throw new AppException("Cannot delete librarian, DB error");
		} else {
			LOG.trace("Librarian deleted successfully, userId --> " + librarianId);
		}

		forward = Constants.Path.SEND_REDIRECT;

		return forward;
	}

}
