package ua.nure.kravchuk.SummaryTask4.web.command.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.exception.AppException;
import ua.nure.kravchuk.SummaryTask4.sevices.UserService;
import ua.nure.kravchuk.SummaryTask4.util.Constants;
import ua.nure.kravchuk.SummaryTask4.web.command.Command;

/**
 * Block user command.
 * 
 * @author P.Kravchuk
 * 
 */
public class BlockUserCommand extends Command {

	private static final long serialVersionUID = -4009995868421195974L;

	private static final Logger LOG = Logger.getLogger(BlockUserCommand.class);

	@Override
	public String logic(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		int userId = Integer.parseInt(request.getParameter("userId"));
		LOG.trace("Request parameter: userId --> " + userId);
		
		boolean isBlocked = !Boolean.parseBoolean(request.getParameter("isBlocked"));
		LOG.trace("Request parameter: isBlocked --> " + isBlocked);

		String forward = Constants.Path.ERROR;

		UserService manager = new UserService();
		boolean result = manager.updateUserIsBlocked(userId, isBlocked);

		if (!result) {
			LOG.debug("Cannot block user, DB error");
			throw new AppException("Cannot block user, DB error");
		} else {
			LOG.trace("User blocked successfully, userId --> " + userId);
		}

		forward = Constants.Path.SEND_REDIRECT;

		return forward;
	}

}
