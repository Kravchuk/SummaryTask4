package ua.nure.kravchuk.SummaryTask4.web.command.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.exception.AppException;
import ua.nure.kravchuk.SummaryTask4.repository.entity.User;
import ua.nure.kravchuk.SummaryTask4.sevices.UserService;
import ua.nure.kravchuk.SummaryTask4.util.Constants;
import ua.nure.kravchuk.SummaryTask4.web.command.Command;

public class FindUserCommand extends Command {

	private static final long serialVersionUID = 5107771182126147159L;
	
	private static final Logger LOG = Logger.getLogger(FindUserCommand.class);
	
	@Override
	public String logic(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		int userId = Integer.parseInt(request.getParameter("userId"));
		LOG.trace("Request parameter: userId --> " + userId);
		
		String forward = Constants.Path.ERROR;

		UserService manager = new UserService();
		User user = manager.findUserById(userId);

		if (user == null) {
			LOG.debug("Cannot find user with such id");
			throw new AppException("Cannot find user with such id, DB error");
		} else {
			request.setAttribute("user1", user);
			LOG.trace("user returned, userId --> " + userId);
		}

		forward = Constants.Path.USER_PROFILE;

		return forward;
	}

}
