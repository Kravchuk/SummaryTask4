package ua.nure.kravchuk.SummaryTask4.web.command.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.exception.AppException;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Role;
import ua.nure.kravchuk.SummaryTask4.repository.entity.User;
import ua.nure.kravchuk.SummaryTask4.sevices.UserService;
import ua.nure.kravchuk.SummaryTask4.sevices.web.Validation;
import ua.nure.kravchuk.SummaryTask4.util.Constants;
import ua.nure.kravchuk.SummaryTask4.web.command.Command;

/**
 * Edit user information command.
 * 
 * @author P.Kravchuk
 *
 */
public class EditUserCommand extends Command {

	private static final long serialVersionUID = -5822801405935156736L;

	private static final Logger LOG = Logger.getLogger(EditUserCommand.class);

	@Override
	public String logic(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		String forward = Constants.Path.ERROR;
		UserService manager = new UserService();
		HttpSession session = request.getSession();
		User currentUser = (User) session.getAttribute("user");

		int userId = Integer.parseInt(request.getParameter("userId"));
		LOG.trace("Request parameter: userId --> " + userId);

		if (request.getParameter("begin") != null) {
			forward = Constants.Path.EDIT_USER_PROFILE;

			User user = manager.findUserById(userId);

			session.setAttribute("user1", user);

			LOG.debug("Request parameter: begin != null --> go to edit_user_profile page");
			return forward;
		}

		Validation validation = new Validation();

		String login = request.getParameter("login");
		LOG.trace("Request parameter: login --> " + login);


		validation.checkParameterLogin(login, "login");

		if (!currentUser.getLogin().equals(login)) {
			User user1 = manager.findUserByLogin(login);
			if (user1 != null) {
				LOG.error("User with such login is already exist");
				throw new AppException("We already have user with such login.");
			}

		}
		String name = request.getParameter("name");
		LOG.trace("Request parameter: name --> " + name);
		String surname = request.getParameter("surname");
		LOG.trace("Request parameter: surname --> " + surname);
		String phone = request.getParameter("phone");
		LOG.trace("Request parameter: phone --> " + phone);
		String country = request.getParameter("country");
		LOG.trace("Request parameter: country --> " + country);
		String city = request.getParameter("city");
		LOG.trace("Request parameter: city --> " + city);

		validation.validateAllParametersForEditingUser(name, surname, phone, country, city);
		LOG.debug("All parameters have been validated successfully");

		User user = User.createUser(login, null, null, name, surname, phone, country, city, Role.USER);
		user.setUserId(userId);

		boolean result = manager.updateUser(user);

		if (!result) {
			LOG.debug("Cannot update user");
			throw new AppException("Cannot update user, DB error.");
		} else {
			LOG.trace("User updated successfully, userId --> " + userId);
		}

		forward = Constants.Path.SEND_REDIRECT;

		return forward;

	}



}
