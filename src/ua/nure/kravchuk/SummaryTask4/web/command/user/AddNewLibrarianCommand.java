package ua.nure.kravchuk.SummaryTask4.web.command.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.exception.AppException;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Role;
import ua.nure.kravchuk.SummaryTask4.repository.entity.User;
import ua.nure.kravchuk.SummaryTask4.sevices.UserService;
import ua.nure.kravchuk.SummaryTask4.sevices.web.Validation;
import ua.nure.kravchuk.SummaryTask4.util.Constants;
import ua.nure.kravchuk.SummaryTask4.web.command.Command;

/**
 * Add new user with Role - LIBRARIAN command
 * 
 * @author P.Kravchuk
 *
 */

public class AddNewLibrarianCommand extends Command {

	private static final long serialVersionUID = -1054674069740158000L;
	
	private static final Logger LOG = Logger.getLogger(AddNewLibrarianCommand.class);
	
	@Override
	public String logic(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		String forward = Constants.Path.ERROR;
		UserService manager = new UserService();
		Validation validation = new Validation();

		
		String login = request.getParameter("login");
		LOG.trace("Request parameter: login --> " + login);
		validation.checkParameterLogin(login, "login");

		User user1 = manager.findUserByLogin(login);
		if (user1 != null) {
			LOG.error("User with such login is already exist");
			throw new AppException("We already have user with such login.");
		}

		String email = request.getParameter("email");
		LOG.trace("Request parameter: email --> " + email);
		validation.checkParameterEmail(email, "email");

		User user2 = manager.findUserByEmail(email);
		if (user2 != null) {
			LOG.error("User with such email is already exist");
			throw new AppException("We already have user with such email.");
		}

		String password1 = request.getParameter("password1");
		LOG.trace("Request parameter: password1 --> " + password1);
		String password2 = request.getParameter("password2");
		LOG.trace("Request parameter: password2 --> " + password2);
		String name = request.getParameter("name");
		LOG.trace("Request parameter: name --> " + name);
		String surname = request.getParameter("surname");
		LOG.trace("Request parameter: surname --> " + surname);
		String phone = request.getParameter("phone");
		LOG.trace("Request parameter: phone --> " + phone);
		String country = request.getParameter("country");
		LOG.trace("Request parameter: country --> " + country);
		String city = request.getParameter("city");
		LOG.trace("Request parameter: city --> " + city);

		String role = request.getParameter("role");
		LOG.trace("Request parameter: role --> " + role);
		validation.validateAllParametersForNewUser(password1, password2, name, surname, phone, country, city);
		LOG.debug("All parameters have been validated successfully");

		User user = User.createUser(login, password1, email, name, surname, phone, country, city, Role.valueOf(role));
		
		boolean isAdded = manager.insertUser(user);

		if (isAdded) {
			LOG.trace("New librarian: user --> " + user.toString());
			forward = Constants.Path.SEND_REDIRECT;
		} else {
			LOG.error("Cannot insert new librarian");
			throw new AppException("Cannot insert new librarian");
		}

		return forward;
		
	}

}
