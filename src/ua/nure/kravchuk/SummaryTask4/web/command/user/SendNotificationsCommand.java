package ua.nure.kravchuk.SummaryTask4.web.command.user;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.exception.AppException;
import ua.nure.kravchuk.SummaryTask4.repository.entity.bean.OrderUserBean;
import ua.nure.kravchuk.SummaryTask4.sevices.OrderService;
import ua.nure.kravchuk.SummaryTask4.sevices.web.SendEmail;
import ua.nure.kravchuk.SummaryTask4.util.Constants;
import ua.nure.kravchuk.SummaryTask4.web.command.Command;


/**
 * Send notifications command.
 * 
 * @author P.Kravchuk
 * 
 */
public class SendNotificationsCommand extends Command {

	private static final long serialVersionUID = 4021783765239427135L;
	
	private static final Logger LOG = Logger.getLogger(SendNotificationsCommand.class);

	@Override
	public String logic(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		String forward = Constants.Path.ERROR;
		String begin = request.getParameter("begin");
		OrderService manager = new OrderService();
		
		List<OrderUserBean> emails = manager.findAllEmailsFromExpiredOrders();
		
		if (begin != null) {
			request.setAttribute("emails", emails);
			LOG.trace("Request parameter: orderUserBean list size --> " + emails.size());
			
			forward = Constants.Path.EXPIRED_ORDERS;
			LOG.debug("Request parameter: begin != null --> go to expired_orders page");
			return forward;
		}
		
		SendEmail sendEmail = new SendEmail();
		String url = "http://localhost:8080/SummaryTask4/index.jsp";
		for (int i = 0; i < emails.size(); i++) {
			sendEmail.sendLetterToEmail(emails.get(i).getEmail(), "MyLibrary expired order notification", "MyLibrary expired order notification",
					createMessage(url));
		}
		

		forward = Constants.Path.SEND_REDIRECT;
		
		
		return forward;
	}

	/**
	 * Method for creating message for expired order notification
	 */
	public String createMessage(String url) {
		StringBuilder message = new StringBuilder();

		message.append("Dear user, please return book(s) you took in our library and pay penalty for it.");
		message.append("</p>").append("<p/>").append("<p>");
		message.append(url);
		message.append("</p>").append("<p/>").append("<p>");
		message.append("My Library - Best for you.");

		return message.toString();
	}
	
}
