package ua.nure.kravchuk.SummaryTask4.repository.dao;

import java.util.List;

import ua.nure.kravchuk.SummaryTask4.exception.DBException;
import ua.nure.kravchuk.SummaryTask4.repository.dao.mysql.ConnectionManager;
import ua.nure.kravchuk.SummaryTask4.repository.entity.bean.OrderUserBean;

/**
 * Interface for work with Order User table.
 * 
 * @author P.Kravchuk
 *
 */
public interface OrderUserDao {

	/**
	 * Find all emails of users with expired orders in DB.
	 * 
	 * @param cManager
	 *            connection manager
	 * @return <tt>emails</tt> List with order's return dates and user's emails
	 */
	public List<OrderUserBean> findAllEmailsFromExpiredOrders(ConnectionManager cManager) throws DBException;

}
