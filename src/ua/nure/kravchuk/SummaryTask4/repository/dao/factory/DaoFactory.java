package ua.nure.kravchuk.SummaryTask4.repository.dao.factory;

import ua.nure.kravchuk.SummaryTask4.repository.dao.CatalogDao;
import ua.nure.kravchuk.SummaryTask4.repository.dao.OrderBookDao;
import ua.nure.kravchuk.SummaryTask4.repository.dao.OrderDao;
import ua.nure.kravchuk.SummaryTask4.repository.dao.OrderUserDao;
import ua.nure.kravchuk.SummaryTask4.repository.dao.UserDao;


/**
 * Interface for getting DAO from DB.
 * 
 * @author P.Kravchuk
 *
 */
public interface DaoFactory {

	/**
	 * Method for getting user DAO from DB.
	 * 
	 * @return UserDao user DAO object for work with User table
	 *
	 */
	UserDao getUserDao();

	/**
	 * Method for getting catalog DAO from DB.
	 * 
	 * @return CatalogDao catalog DAO object for work with Catalog table
	 *
	 */
	CatalogDao getCatalogDao();

	/**
	 * Method for getting order DAO from DB.
	 * 
	 * @return OrderDao order DAO object for work with Order table
	 *
	 */
	OrderDao getOrderDao();
	
	/**
	 * Method for getting orderBook DAO from DB.
	 * 
	 * @return OrderBookDao order DAO object for work with OrderBook table
	 *
	 */
	OrderBookDao getOrderBookDao();
	
	/**
	 * Method for getting orderUser DAO from DB.
	 * 
	 * @return OrderUserDao order DAO object for work with OrderUser table
	 *
	 */
	OrderUserDao getOrderUserDao();

}
