package ua.nure.kravchuk.SummaryTask4.repository.dao;

import java.util.List;

import ua.nure.kravchuk.SummaryTask4.exception.DBException;
import ua.nure.kravchuk.SummaryTask4.repository.dao.mysql.ConnectionManager;
import ua.nure.kravchuk.SummaryTask4.repository.entity.bean.OrderBookBean;

/**
 * Interface for work with Order Book table.
 * 
 * @author P.Kravchuk
 *
 */
public interface OrderBookDao {

	/**
	 * Find order book bean by user id in DB.
	 * 
	 * @param cManager
	 *            connection manager
	 * @param userId
	 *            user object id
	 * @param isActiveOrders
	 *            set <tt>true</tt> to get only active orders of the user with
	 *            specified <tt>userId</tt>
	 * @return <tt>orders</tt> List with orders
	 */
	public List<OrderBookBean> findOrdersByUserId(ConnectionManager cManager, int userId, boolean isActiveOrders)
			throws DBException;

}
