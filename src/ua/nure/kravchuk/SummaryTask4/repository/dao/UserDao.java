package ua.nure.kravchuk.SummaryTask4.repository.dao;

import java.util.List;

import ua.nure.kravchuk.SummaryTask4.exception.DBException;
import ua.nure.kravchuk.SummaryTask4.repository.dao.mysql.ConnectionManager;
import ua.nure.kravchuk.SummaryTask4.repository.entity.User;
import ua.nure.kravchuk.SummaryTask4.repository.entity.bean.UserBookBean;

/**
 * Interface for work with User table.
 * 
 * @author P.Kravchuk
 *
 */
public interface UserDao {

	/**
	 * Insert user to DB.
	 * 
	 * @param cManager
	 *            connection manager
	 * @param user
	 *            user object
	 * @return <tt>true</tt> if user was added
	 */
	public boolean insertUser(ConnectionManager cManager, User user) throws DBException;

	/**
	 * Update existing user in DB.
	 * 
	 * @param cManager
	 *            connection manager
	 * @param user
	 *            user object
	 * @return <tt>true</tt> if user was updated
	 */
	public boolean updateUser(ConnectionManager cManager, User user) throws DBException;

	/**
	 * Update existing user <tt>isBlocked</tt> status in DB.
	 * 
	 * @param cManager
	 *            connection manager
	 * @param userId
	 *            user object id
	 * @param isBlocked
	 *            new user object status
	 * @return <tt>true</tt> if user was updated
	 */
	public boolean updateUserIsBlocked(ConnectionManager cManager, int userId, boolean isBlocked) throws DBException;

	/**
	 * Find user in DB by specified login or email or by login and password or
	 * by email and password.
	 * 
	 * @param cManager
	 *            connection manager
	 * @param login
	 *            user's login
	 * @param email
	 *            user's email
	 * @param password
	 *            user's password
	 * @return <tt>user</tt> user object
	 */
	public User findUserByLoginOrEmailAndPassword(ConnectionManager cManager, String login, String email,
			String password) throws DBException;

	/**
	 * Find user in DB by userId.
	 * 
	 * @param cManager
	 *            connection manager
	 * @param userId
	 *            user's id
	 * @return <tt>user</tt> user object
	 */
	public User findUserById(ConnectionManager cManager, int userId) throws DBException;

	/**
	 * Find all users in DB.
	 * 
	 * @param cManager
	 *            connection manager
	 * @param isLibrarian
	 *            set <tt>true</tt> to find librarians
	 * @return <tt>users</tt> List with users
	 */
	public List<User> findAllUsers(ConnectionManager cManager, boolean isLibrarian) throws DBException;

	/**
	 * Delete existing user in DB.
	 * 
	 * @param cManager
	 *            connection manager
	 * @param userId
	 *            user object id
	 * @return <tt>true</tt> if user was deleted
	 */
	public boolean deleteUserById(ConnectionManager cManager, int userId) throws DBException;

	
	/**
	 */
	public List<UserBookBean> findAllUsers2(ConnectionManager cManager)throws DBException;
}
