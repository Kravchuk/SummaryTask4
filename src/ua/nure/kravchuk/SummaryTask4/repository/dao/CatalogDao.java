package ua.nure.kravchuk.SummaryTask4.repository.dao;

import java.util.List;

import ua.nure.kravchuk.SummaryTask4.exception.DBException;
import ua.nure.kravchuk.SummaryTask4.repository.dao.mysql.ConnectionManager;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Book;

/**
 * Interface for work with Catalog table.
 * 
 * @author P.Kravchuk
 *
 */
public interface CatalogDao {

	/**
	 * Insert book to DB.
	 * 
	 * @param cManager
	 *            connection manager
	 * @param book
	 *            book object
	 * @return <tt>true</tt> if book was added
	 */
	public boolean insertBook(ConnectionManager cManager, Book book) throws DBException;

	/**
	 * Update existing book in DB.
	 * 
	 * @param cManager
	 *            connection manager
	 * @param bookId
	 *            book object id
	 * @param book
	 *            book object
	 * @return <tt>true</tt> if book was updated
	 */
	public boolean updateBook(ConnectionManager cManager, int bookId, Book book) throws DBException;

	/**
	 * Delete existing book in DB.
	 * 
	 * @param cManager
	 *            connection manager
	 * @param bookId
	 *            book object id
	 * @return <tt>true</tt> if book was deleted
	 */
	public boolean deleteBookById(ConnectionManager cManager, int bookId) throws DBException;

	/**
	 * Find book in DB.
	 * 
	 * @param cManager
	 *            connection manager
	 * @param bookId
	 *            book object id
	 * @return <tt>book</tt> book object with specified <tt>bookId</tt>
	 */
	public Book findBookById(ConnectionManager cManager, int bookId) throws DBException;

	/**
	 * Find books in DB by specified parameters.
	 * 
	 * @param cManager
	 *            connection manager
	 * @param name
	 *            book's name
	 * @param author
	 *            book's author
	 * @param publisher
	 *            book's publisher
	 * @param genre
	 *            book's genre
	 * @param date
	 *            book's publication date
	 * @param orderBy
	 *            can be set as <tt>name</tt>, <tt>author</tt>,
	 *            <tt>publisher</tt>, <tt>publicationDate</tt>,
	 *            <tt>popularity</tt> <tt>naturalOrder</tt> if you want this
	 *            list was sorted by this parameter
	 * @param reverse
	 *            set <tt>true</tt> to get reverse sorted list
	 * @return <tt>books</tt> List with books
	 */
	public List<Book> findBooksByParameters(ConnectionManager cManager, String name, String author, String publisher,
			String genre, String date, String orderBy, boolean reverse) throws DBException;

	/**
	 * Find all books in DB.
	 * 
	 * @param cManager
	 *            connection manager
	 * @param orderByParameter
	 *            can be set as <tt>name</tt>, <tt>author</tt>,
	 *            <tt>publisher</tt>, <tt>publicationDate</tt>,
	 *            <tt>popularity</tt> <tt>naturalOrder</tt> if you want this
	 *            list was sorted by this parameter
	 * @return <tt>books</tt> List with books
	 */
	public List<Book> findAllBooks(ConnectionManager cManager, String orderByParameter) throws DBException;

}
