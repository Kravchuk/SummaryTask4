package ua.nure.kravchuk.SummaryTask4.repository.dao;

import java.util.List;

import ua.nure.kravchuk.SummaryTask4.exception.DBException;
import ua.nure.kravchuk.SummaryTask4.repository.dao.mysql.ConnectionManager;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Order;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Status;

/**
 * Interface for work with Order table.
 * 
 * @author P.Kravchuk
 *
 */
public interface OrderDao {

	/**
	 * Insert order to DB.
	 * 
	 * @param cManager
	 *            connection manager
	 * @param order
	 *            order object
	 * @return <tt>true</tt> if order was added
	 */
	public boolean makeOrder(ConnectionManager cManager, Order order) throws DBException;

	/**
	 * Find order in DB.
	 * 
	 * @param cManager
	 *            connection manager
	 * @param orderId
	 *            order object id
	 * @return <tt>order</tt> order object with specified <tt>orderId</tt>
	 */
	public Order findOrderById(ConnectionManager cManager, int orderId) throws DBException;

	/**
	 * Find order by user id in DB.
	 * 
	 * @param cManager
	 *            connection manager
	 * @param userId
	 *            user object id
	 * @param isActiveOrders
	 *            set <tt>true</tt> to get only active orders of the user with
	 *            specified <tt>userId</tt>
	 * @return <tt>orders</tt> List with orders
	 */
	public List<Order> findOrdersByUserId(ConnectionManager cManager, int userId, boolean isActiveOrders)
			throws DBException;
	
	/**
	 * Delete order by order id in DB.
	 * 
	 * @param cManager
	 *            connection manager
	 * @param orderId
	 *            order object id
	 * @return <tt>true</tt> if order was deleted
	 */
	public boolean 	deleteOrderById(ConnectionManager cManager, int orderId)
			throws DBException;


	/**
	 * Find all orders in DB.
	 * 
	 * @param cManager
	 *            connection manager
	 * @param orderByReturnDate
	 *            set <tt>true</tt> to get sorted orders by book's
	 *            <tt>returnDate</tt>
	 * @return <tt>orders</tt> List with orders
	 */
	public List<Order> findAllOrders(ConnectionManager cManager, boolean orderByReturnDate) throws DBException;

	/**
	 * Find waiting orders in DB.
	 * 
	 * @param cManager
	 *            connection manager
	 * @return <tt>orders</tt> List with confirmation awaiting orders
	 */
	public List<Order> findAllWaitingOrders(ConnectionManager cManager) throws DBException;
	

	/**
	 * Update order <tt>status</tt> in DB.
	 * 
	 * @param cManager
	 *            connection manager
	 * @param orderId
	 *            order object id
	 * @param status
	 *            order object status
	 * @return <tt>true</tt> if order was updated
	 */
	public boolean updateOrderStatus(ConnectionManager cManager, int orderId, Status status) throws DBException;

	
	public List<Order> findAllOrders2(ConnectionManager cManager) throws DBException;
}
