package ua.nure.kravchuk.SummaryTask4.repository.dao.mysql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.exception.DBException;
import ua.nure.kravchuk.SummaryTask4.util.Constants;

/**
 * Connection manager class.
 * 
 * @author P.Kravchuk
 *
 */

public class ConnectionManager {
	private static DataSource ds;

	private static final Logger LOG = Logger.getLogger(ConnectionManager.class);

	static {
		try {
			if (ds == null) {
				initializeDataSource();
			}
		} catch (DBException e) {
			LOG.error(Constants.DBErrors.INITIALIZE_DATASOURCE, e);
			throw new DBException(Constants.DBErrors.INITIALIZE_DATASOURCE);
		}
		Properties prop = new Properties();

		prop.setProperty("charSet", "UTF-8");
		LOG.trace("Set property for encoding --> " + prop.getProperty("charSet"));
	}

	/**
	 * Returns a DB connection from the Pool Connections. Before using this
	 * method you must configure the Date Source and the Connections Pool in
	 * your WEB_APP_ROOT/META-INF/context.xml file.
	 * 
	 * @return <tt>con</tt> DB Connection.
	 */
	public Connection getConnection() throws DBException {
		Connection con = null;
		try {
			con = ds.getConnection();
			LOG.debug("Getting connection --> " + con);
		} catch (SQLException e) {
			LOG.error(Constants.DBErrors.OBTAIN_CONNECTION, e);
			throw new DBException(Constants.DBErrors.OBTAIN_CONNECTION, e);
		}
		return con;
	}

	private static void initializeDataSource() throws DBException {
		Context initCtx;
		try {
			initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:/comp/env");
			ds = (DataSource) envCtx.lookup("jdbc/library");
			LOG.debug("Initialized data source --> " + ds);
		} catch (NamingException e) {
			LOG.error(Constants.DBErrors.INITIALIZE_CONTEXT, e);
			throw new DBException(Constants.DBErrors.INITIALIZE_CONTEXT, e);
		}
	}

	/**
	 * Method for canceling all changes.
	 * 
	 * @param c
	 *            DB Connection
	 *
	 */
	public void safeRollback(Connection c) throws DBException {
		if (c != null) {
			try {
				c.rollback();
			} catch (SQLException e) {
				LOG.error(Constants.DBErrors.ROLLBACK_CONNECTION, e);
				throw new DBException(Constants.DBErrors.ROLLBACK_CONNECTION);
			}
		}
	}

	/**
	 * Method for closing ResultSet.
	 * 
	 * @param rs
	 *            DB ResultSet
	 *
	 */
	public void close(ResultSet rs) throws DBException {
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			LOG.error(Constants.DBErrors.CLOSE_RESULTSET, e);
			throw new DBException(Constants.DBErrors.CLOSE_RESULTSET, e);
		}
	}

	/**
	 * Method for closing statement.
	 * 
	 * @param stmt
	 *            DB Statement
	 *
	 */
	public void close(Statement stmt) throws DBException {
		try {
			if (stmt != null) {
				stmt.close();
			}

		} catch (SQLException e) {
			LOG.error(Constants.DBErrors.CLOSE_STATEMENT, e);
			throw new DBException(Constants.DBErrors.CLOSE_STATEMENT, e);
		}
	}

	/**
	 * Method for closing connection.
	 * 
	 * @param con
	 *            DB Connection
	 *
	 */
	public void close(Connection con) throws DBException {
		try {
			if (con != null) {
				con.close();
			}

		} catch (SQLException e) {
			LOG.error(Constants.DBErrors.CLOSE_CONNECTION, e);
			throw new DBException(Constants.DBErrors.CLOSE_CONNECTION, e);
		}
	}
}
