package ua.nure.kravchuk.SummaryTask4.repository.dao.mysql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.repository.dao.OrderUserDao;
import ua.nure.kravchuk.SummaryTask4.exception.DBException;
import ua.nure.kravchuk.SummaryTask4.repository.entity.bean.OrderUserBean;
import ua.nure.kravchuk.SummaryTask4.repository.dao.mysql.ConnectionManager;
import ua.nure.kravchuk.SummaryTask4.util.Constants;

/**
 * Class for work with OrderUser table. Works with MySql DB. Only the required
 * DAO methods are defined!
 * 
 * @author P.Kravchuk
 *
 */
public class MySqlOrderUserDao implements OrderUserDao {

	private static final Logger LOG = Logger.getLogger(MySqlOrderUserDao.class);

	private Connection con;
	private ConnectionManager instance;


	public MySqlOrderUserDao() {
		// no op
	}

	@Override
	public List<OrderUserBean> findAllEmailsFromExpiredOrders(ConnectionManager cManager) throws DBException {
		instance = cManager;
		List<OrderUserBean> emails = null;
		try {
			con = instance.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			emails = findAllEmailsFromExpiredOrders(con);

			return emails;
		} catch (SQLException ex) {
			LOG.error(Constants.DBErrors.FIND_ORDERS_BY_USER_ID, ex);
			throw new DBException(Constants.DBErrors.FIND_ORDERS_BY_USER_ID, ex);
		} finally {
			instance.close(con);
		}
	}

	private List<OrderUserBean> findAllEmailsFromExpiredOrders(Connection con)
			throws DBException {
		List<OrderUserBean> emails = new ArrayList<>();
		Statement stmt = null;
		ResultSet rs = null;

		try {
			stmt = con.createStatement();
			rs = stmt.executeQuery(Constants.Sql.SELECT_EMAILS_OF_USERS_WITH_EXPIRED_ORDERS);
			
			Date currentDate = new Date();
			while (rs.next()) {
				OrderUserBean email = extractOrderUserBean(rs);
				if(currentDate.compareTo(email.getReturnDate()) > -1){
					emails.add(email);
				}
			}
			
			return emails;
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			return emails;
		} finally {
			instance.close(rs);
			instance.close(stmt);
		}
	}

	private OrderUserBean extractOrderUserBean(ResultSet rs) throws SQLException {
		OrderUserBean orderUserBean = new OrderUserBean();
		
		orderUserBean.setUserId(rs.getInt("user_id"));
		orderUserBean.setEmail(rs.getString("email"));
		orderUserBean.setOrderId(rs.getInt("order_id"));
		orderUserBean.setReturnDate(rs.getDate("return_date"));
		
		return orderUserBean;
	}



}
