package ua.nure.kravchuk.SummaryTask4.repository.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.repository.dao.CatalogDao;
import ua.nure.kravchuk.SummaryTask4.exception.DBException;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Book;
import ua.nure.kravchuk.SummaryTask4.repository.dao.mysql.ConnectionManager;
import ua.nure.kravchuk.SummaryTask4.util.Constants;

/**
 * Class for work with Catalog table. Works with MySql DB. Only the required DAO
 * methods are defined!
 * 
 * @author P.Kravchuk
 *
 */
public class MySqlCatalogDao implements CatalogDao {

	private static final Logger LOG = Logger.getLogger(MySqlCatalogDao.class);

	private Connection con;
	private ConnectionManager instance;

	public MySqlCatalogDao() {
		// no op
	}

	@Override
	public boolean insertBook(ConnectionManager cManager, Book book) throws DBException {
		instance = cManager;
		try {
			con = instance.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			boolean res = insertBook(con, book);

			con.commit();

			return res;
		} catch (SQLException ex) {

			instance.safeRollback(con);

			LOG.error(Constants.DBErrors.INSERT_BOOK, ex);
			throw new DBException(Constants.DBErrors.INSERT_BOOK, ex);
		} finally {
			instance.close(con);
		}
	}

	private boolean insertBook(Connection con, Book book) throws DBException {
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement(Constants.Sql.INSERT_BOOK);
			int k = 0;
			pstmt.setString(++k, book.getName());
			pstmt.setString(++k, book.getAuthor());
			pstmt.setString(++k, book.getPublisher());
			pstmt.setString(++k, book.getGenre());
			pstmt.setDate(++k, new java.sql.Date(book.getPublicationDate().getTime()));
			pstmt.setInt(++k, book.getTotalAmount());
			pstmt.setInt(++k, book.getAvailable());
			pstmt.setInt(++k, book.getNumberOfDaysForUse());
			pstmt.setDouble(++k, book.getPenaltyPayment());
			pstmt.executeUpdate();
			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			instance.close(pstmt);
		}

	}

	@Override
	public boolean updateBook(ConnectionManager cManager, int bookId, Book book) throws DBException {
		instance = cManager;
		try {
			con = instance.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			int res = updateBook(con, bookId, book);
			System.out.println(res);
			con.commit();

			return res > 0;
		} catch (SQLException ex) {

			instance.safeRollback(con);

			LOG.error(Constants.DBErrors.UPDATE_BOOK, ex);
			throw new DBException(Constants.DBErrors.UPDATE_BOOK, ex);
		} finally {
			instance.close(con);
		}
	}

	private int updateBook(Connection con, int bookId, Book book) throws DBException {
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement(Constants.Sql.UPDATE_BOOK);
			int k = 0;
			pstmt.setString(++k, book.getName());
			pstmt.setString(++k, book.getAuthor());
			pstmt.setString(++k, book.getPublisher());
			pstmt.setString(++k, book.getGenre());
			pstmt.setDate(++k, new java.sql.Date(book.getPublicationDate().getTime()));
			pstmt.setInt(++k, book.getTotalAmount());
			pstmt.setInt(++k, book.getAvailable());
			pstmt.setInt(++k, book.getNumberOfDaysForUse());
			pstmt.setDouble(++k, book.getPenaltyPayment());
			pstmt.setInt(++k, book.getNumberOfReadings());
			pstmt.setInt(++k, bookId);
			int res = pstmt.executeUpdate();
			return res;
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			return 0;
		} finally {
			instance.close(pstmt);
		}

	}

	@Override
	public boolean deleteBookById(ConnectionManager cManager, int bookId) throws DBException {
		instance = cManager;

		try {
			con = instance.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			int res = deleteBookById(con, bookId);

			con.commit();
			return res > 0;
		} catch (SQLException ex) {

			instance.safeRollback(con);

			LOG.error(Constants.DBErrors.DELETE_BOOK, ex);
			throw new DBException(Constants.DBErrors.DELETE_BOOK, ex);
		} finally {
			instance.close(con);
		}
	}

	private int deleteBookById(Connection con, int bookId) throws DBException {
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement(Constants.Sql.DELETE_BOOK_BY_ID);
			int k = 0;
			pstmt.setInt(++k, bookId);

			int res = pstmt.executeUpdate();

			return res;
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			return 0;
		} finally {
			instance.close(pstmt);
		}

	}

	@Override
	public Book findBookById(ConnectionManager cManager, int bookId) throws DBException {
		instance = cManager;
		Book book = null;
		try {
			con = instance.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			book = findBookById(con, bookId);

			return book;
		} catch (SQLException ex) {
			LOG.error(Constants.DBErrors.FIND_BOOK_BY_ID, ex);
			throw new DBException(Constants.DBErrors.FIND_BOOK_BY_ID, ex);
		} finally {
			instance.close(con);
		}
	}

	private Book findBookById(Connection con, int bookId) throws DBException {
		Book book = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = con.prepareStatement(Constants.Sql.SELECT_BOOK_BY_ID);
			int k = 0;
			pstmt.setInt(++k, bookId);

			rs = pstmt.executeQuery();
			if (rs.next()) {
				book = extractBook(rs);
			}
			return book;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return book;
		} finally {
			instance.close(rs);
			instance.close(pstmt);
		}
	}

	@Override
	public List<Book> findBooksByParameters(ConnectionManager cManager, String name, String author, String publisher,
			String genre, String date, String orderBy, boolean reverse) throws DBException {
		instance = cManager;
		List<Book> books = null;
		try {
			con = instance.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			books = findBooksByParameters(con, name, author, publisher, genre, date, orderBy, reverse);

			return books;
		} catch (SQLException ex) {
			LOG.error(Constants.DBErrors.FIND_BOOKS_BY_PRAMETERS, ex);
			 throw new DBException(Constants.DBErrors.FIND_BOOKS_BY_PRAMETERS, ex);
		} finally {
			instance.close(con);
		}
	}

	private List<Book> findBooksByParameters(Connection con, String name, String author, String publisher, String genre,
			String date, String orderBy, boolean reverse) throws DBException {
		List<Book> books = new ArrayList<>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			pstmt = prepareForFindingBook(pstmt, name, author, publisher, genre, date, orderBy, reverse);

			rs = pstmt.executeQuery();
			while (rs.next()) {
				Book book = extractBook(rs);
				books.add(book);
			}

			return books;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return books;
		} finally {
			instance.close(rs);
			instance.close(pstmt);
		}
	}

	private PreparedStatement prepareForFindingBook(PreparedStatement prstmt, String name, String author,
			String publisher, String genre, String date, String orderByParameter, boolean reverse) throws SQLException {

		PreparedStatement pstmt = prstmt;
		int k = 0;
		StringBuilder statement = new StringBuilder(Constants.Sql.SELECT_BOOK_BY_PARAMETERS);
		String[] params = new String[6];

		if (name != null) {
			statement.append("name=?");
			params[k] = name;
			k++;
		} else if (author != null) {
			if (k != 0) {
				statement.append(" AND ");
			}
			statement.append("author=?");
			params[k] = author;
			k++;
		} else if (publisher != null) {
			if (k != 0) {
				statement.append(" AND ");
			}
			statement.append("publisher=?");
			params[k] = publisher;
			k++;
		} else if (genre != null) {
			if (k != 0) {
				statement.append(" AND ");
			}
			statement.append("genre=?");
			params[k] = genre;
			k++;
		} else if (date != null) {
			if (k != 0) {
				statement.append(" AND ");
			}
			statement.append("publication_date=?");
			params[k] = date;
			k++;
		}

		if(k==0){
			statement.delete(33, 40);
		}
		
		statement.append(" ORDER BY ");
		
		if (orderByParameter.equals("name")) {
			statement.append("name");

		} else if (orderByParameter.equals("author")) {
			statement.append("author");
		} else if (orderByParameter.equals("publisher")) {
			statement.append("publisher");
		} else if (orderByParameter.equals("publicationDate")) {
			statement.append("publication_date");
		} else if (orderByParameter.equals("popularity")) {
			statement.append("number_of_readings");
		} else if (orderByParameter.equals("naturalOrder")) {
			statement.append("book_id");
		} 
		
		if(reverse){
			statement.append(" DESC");
		}
		

		k = 0;

		pstmt = con.prepareStatement(statement.toString());

		for (int i = 0; i < params.length; i++) {
			if(params[i] != null){
				pstmt.setString(++k, params[i]);
			}
		}

		return pstmt;
	}

	@Override
	public List<Book> findAllBooks(ConnectionManager cManager, String orderByParameter) throws DBException {
		instance = cManager;
		List<Book> books = null;
		try {
			con = instance.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			books = findAllBooks(con, orderByParameter);

			return books;
		} catch (SQLException ex) {
			LOG.error(Constants.DBErrors.FIND_ALL_BOOKS, ex);
			throw new DBException(Constants.DBErrors.FIND_ALL_BOOKS, ex);
		} finally {
			instance.close(con);
		}
	}

	private List<Book> findAllBooks(Connection con, String orderByParameter) throws DBException, SQLException {
		List<Book> books = new ArrayList<>();
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = con.createStatement();

			rs = prepareForFindingAllBooks(rs, stmt, orderByParameter);

			while (rs.next()) {
				Book book = extractBook(rs);
				books.add(book);
			}
			return books;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return books;
		} finally {
			instance.close(rs);
			instance.close(stmt);
		}
	}

	private ResultSet prepareForFindingAllBooks(ResultSet rst, Statement stmnt, String orderByParameter)
			throws SQLException {
		Statement stmt = stmnt;
		ResultSet rs = rst;

		if (orderByParameter.equals("name")) {
			rs = stmt.executeQuery(Constants.Sql.SELECT_SORTED_CATLOG_BY_NAME);
		} else if (orderByParameter.equals("author")) {
			rs = stmt.executeQuery(Constants.Sql.SELECT_SORTED_CATLOG_BY_AUTHOR);
		} else if (orderByParameter.equals("publisher")) {
			rs = stmt.executeQuery(Constants.Sql.SELECT_SORTED_CATLOG_BY_PUBLISHER);
		} else if (orderByParameter.equals("publicationDate")) {
			rs = stmt.executeQuery(Constants.Sql.SELECT_SORTED_CATLOG_BY_PUBLICATION_DATE);
		} else if (orderByParameter.equals("popularity")) {
			rs = stmt.executeQuery(Constants.Sql.SELECT_SORTED_CATLOG_BY_POPULARITY);
		} else if (orderByParameter.equals("naturalOrder")) {
			rs = stmt.executeQuery(Constants.Sql.SELECT_CATLOG);
		}

		return rs;
	}

	private Book extractBook(ResultSet rs) throws SQLException {
		Book book = new Book();
		book.setBookId(rs.getInt("book_id"));
		book.setName(rs.getString("name"));
		book.setAuthor(rs.getString("author"));
		book.setPublisher(rs.getString("publisher"));
		book.setGenre(rs.getString("genre"));
		book.setPublicationDate(rs.getDate("publication_date"));
		book.setTotalAmount(rs.getInt("total_amount"));
		book.setAvailable(rs.getInt("available"));
		book.setNumberOfDaysForUse(rs.getInt("number_of_days_for_use"));
		book.setPenaltyPayment(rs.getDouble("penalty_payment"));
		book.setNumberOfReadings(rs.getInt("number_of_readings"));
		return book;
	}

}
