package ua.nure.kravchuk.SummaryTask4.repository.dao.mysql;

import ua.nure.kravchuk.SummaryTask4.repository.dao.CatalogDao;
import ua.nure.kravchuk.SummaryTask4.repository.dao.OrderBookDao;
import ua.nure.kravchuk.SummaryTask4.repository.dao.OrderDao;
import ua.nure.kravchuk.SummaryTask4.repository.dao.OrderUserDao;
import ua.nure.kravchuk.SummaryTask4.repository.dao.UserDao;
import ua.nure.kravchuk.SummaryTask4.repository.dao.factory.DaoFactory;

/**
* Class for getting DAO from DB.
* 
* @author P.Kravchuk
*
*/
public class MySqlDaoFactory implements DaoFactory {
	
	private static MySqlDaoFactory instance;

	public static synchronized MySqlDaoFactory getInstance() {
		if (instance == null) {
			instance = new MySqlDaoFactory();
		}
		return instance;
	}

	public MySqlDaoFactory() {
		// no op
	}


	@Override
	public UserDao getUserDao() {
		return new MySqlUserDao();
	}

	@Override
	public CatalogDao getCatalogDao() {
		return new MySqlCatalogDao();
	}

	@Override
	public OrderDao getOrderDao() {
		return new MySqlOrderDao();
	}


	@Override
	public OrderBookDao getOrderBookDao() {
		return new MySqlOrderBookDao();
	}

	@Override
	public OrderUserDao getOrderUserDao() {
		return new MySqlOrderUserDao();
	}

}
