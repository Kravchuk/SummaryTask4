package ua.nure.kravchuk.SummaryTask4.repository.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.repository.dao.UserDao;
import ua.nure.kravchuk.SummaryTask4.exception.DBException;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Role;
import ua.nure.kravchuk.SummaryTask4.repository.entity.User;
import ua.nure.kravchuk.SummaryTask4.repository.entity.bean.UserBookBean;
import ua.nure.kravchuk.SummaryTask4.repository.dao.mysql.ConnectionManager;
import ua.nure.kravchuk.SummaryTask4.util.Constants;

/**
 * Class for work with User table. Works with MySql DB. Only the required DAO
 * methods are defined!
 * 
 * @author P.Kravchuk
 *
 */
public class MySqlUserDao implements UserDao {

	private static final Logger LOG = Logger.getLogger(MySqlUserDao.class);

	private Connection con;
	private ConnectionManager instance;

	public MySqlUserDao() {
		// no op
	}

	@Override
	public boolean insertUser(ConnectionManager cManager, User user) throws DBException {
		instance = cManager;
		try {
			con = instance.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			
			boolean res = insertUser(con, user);

			con.commit();

			return res;
		} catch (SQLException ex) {

			instance.safeRollback(con);

			LOG.error(Constants.DBErrors.INSERT_USER, ex);
			throw new DBException(Constants.DBErrors.INSERT_USER, ex);
		} finally {
			instance.close(con);
		}
	}

	private boolean insertUser(Connection con, User user) throws DBException {
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement(Constants.Sql.INSERT_USER);
			int k = 0;
			pstmt.setString(++k, user.getLogin());
			pstmt.setString(++k, user.getPassword());
			pstmt.setString(++k, user.getEmail());
			pstmt.setString(++k, user.getName());
			pstmt.setString(++k, user.getSurname());
			pstmt.setString(++k, user.getPhone());
			pstmt.setString(++k, user.getCountry());
			pstmt.setString(++k, user.getCity());
			pstmt.setDate(++k, new java.sql.Date(user.getRegistrationDate().getTime()));
			pstmt.setBoolean(++k, user.isBlocked());
			pstmt.setString(++k, user.getRole().getName());
			pstmt.executeUpdate();
			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			instance.close(pstmt);
		}

	}

	@Override
	public boolean updateUser(ConnectionManager cManager, User user) throws DBException {
		instance = cManager;
		PreparedStatement pstmt = null;
		try {
			con = instance.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			int res = updateUser(con, user);

			con.commit();

			return res != 0;
		} catch (SQLException ex) {

			instance.safeRollback(con);

			LOG.error(Constants.DBErrors.UPDATE_USER, ex);
			throw new DBException(Constants.DBErrors.UPDATE_USER, ex);
		} finally {
			instance.close(pstmt);
			instance.close(con);
		}
	}

	private int updateUser(Connection con, User user) throws DBException {
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement(Constants.Sql.UPDATE_USER);
			int k = 0;
			pstmt.setString(++k, user.getLogin());
			pstmt.setString(++k, user.getName());
			pstmt.setString(++k, user.getSurname());
			pstmt.setString(++k, user.getPhone());
			pstmt.setString(++k, user.getCountry());
			pstmt.setString(++k, user.getCity());
			pstmt.setInt(++k, user.getUserId());

			int res = pstmt.executeUpdate();

			return res;
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			return 0;
		} finally {
			instance.close(pstmt);
		}

	}

	@Override
	public boolean updateUserIsBlocked(ConnectionManager cManager, int userId, boolean isBlocked) throws DBException {
		instance = cManager;
		PreparedStatement pstmt = null;
		try {
			con = instance.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			int res = updateUserIsBlocked(con, userId, isBlocked);

			con.commit();

			return res != 0;
		} catch (SQLException ex) {

			instance.safeRollback(con);

			LOG.error(Constants.DBErrors.UPDATE_USER_IS_BLOCKED, ex);
			throw new DBException(Constants.DBErrors.UPDATE_USER_IS_BLOCKED, ex);
		} finally {
			instance.close(pstmt);
			instance.close(con);
		}
	}

	private int updateUserIsBlocked(Connection con, int userId, boolean isBlocked) throws DBException {
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement(Constants.Sql.UPDATE_USER_IS_BLOCKED);
			int k = 0;
			pstmt.setBoolean(++k, isBlocked);
			pstmt.setInt(++k, userId);

			int res = pstmt.executeUpdate();

			return res;
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			return 0;
		} finally {
			instance.close(pstmt);
		}

	}

	@Override
	public User findUserByLoginOrEmailAndPassword(ConnectionManager cManager, String login, String email,
			String password) throws DBException {
		instance = cManager;
		User user = null;
		try {
			con = instance.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			user = findUserByLEP(con, login, email, password);

			return user;
		} catch (SQLException ex) {
			LOG.error(Constants.DBErrors.FIND_USER, ex);
			throw new DBException(Constants.DBErrors.FIND_USER, ex);
		} finally {
			instance.close(con);
		}

	}

	private User findUserByLEP(Connection con, String login, String email, String password) throws DBException {
		User user = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {

			pstmt = prepareForFindingUserByLEP(pstmt, login, email, password);
			
			rs = pstmt.executeQuery();
			if (rs.next()) {
				user = extractUser(rs);
			}
			return user;
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			return null;
		} finally {
			instance.close(rs);
			instance.close(pstmt);
		}
	}

	private PreparedStatement prepareForFindingUserByLEP(PreparedStatement pstmnt, String login, String email, String password) throws SQLException{
		PreparedStatement pstmt = pstmnt;
		
		if (password != null) {
			if (login != null) {
				pstmt = con.prepareStatement(Constants.Sql.SELECT_USER_BY_LOGIN_AND_PASSWORD);
				pstmt.setString(1, login);
				pstmt.setString(2, password);
			} else {
				pstmt = con.prepareStatement(Constants.Sql.SELECT_USER_BY_EMAIL_AND_PASSWORD);
				pstmt.setString(1, email);
				pstmt.setString(2, password);
			}
		} else {
			if (login != null) {
				pstmt = con.prepareStatement(Constants.Sql.SELECT_USER_BY_LOGIN);
				pstmt.setString(1, login);
			} else {
				pstmt = con.prepareStatement(Constants.Sql.SELECT_USER_BY_EMAIL);
				pstmt.setString(1, email);
			}
		}
		
		return pstmt;
	}
	
	
	@Override
	public User findUserById(ConnectionManager cManager, int userId) throws DBException {
		instance = cManager;
		User user = null;
		try {
			con = instance.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			user = findUserById(con, userId);

			return user;
		} catch (SQLException ex) {
			LOG.error(Constants.DBErrors.FIND_USER, ex);
			throw new DBException(Constants.DBErrors.FIND_USER, ex);
		} finally {
			instance.close(con);
		}

	}

	private User findUserById(Connection con, int userId) throws DBException {
		User user = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {

			pstmt = con.prepareStatement(Constants.Sql.SELECT_USER_BY_ID);
			int k = 0;
			pstmt.setInt(++k, userId);
			
			rs = pstmt.executeQuery();
			if (rs.next()) {
				user = extractUser(rs);
			}
			return user;
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			return null;
		} finally {
			instance.close(rs);
			instance.close(pstmt);
		}
	}
	
	@Override
	public List<User> findAllUsers(ConnectionManager cManager, boolean isLibrarians) throws DBException {
		instance = cManager;
		List<User> users = null;
		try {
			con = instance.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			users = findAllUsers(con, isLibrarians);

			return users;
		} catch (SQLException ex) {
			LOG.error(Constants.DBErrors.FIND_ALL_USERS, ex);
			throw new DBException(Constants.DBErrors.FIND_ALL_USERS, ex);
		} finally {
			instance.close(con);
		}
	}

	private List<User> findAllUsers(Connection con, boolean isLibrarians) throws DBException {
		List<User> users = new ArrayList<>();
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = con.createStatement();

			if (isLibrarians) {
				rs = stmt.executeQuery(Constants.Sql.SELECT_ALL_LIBRARIANS);
			} else {
				rs = stmt.executeQuery(Constants.Sql.SELECT_ALL_USERS);
			}

			while (rs.next()) {
				User user = extractUser(rs);
				users.add(user);
			}
			return users;
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			return null;
		} finally {
			instance.close(rs);
			instance.close(stmt);
		}
	}

	
	@Override
	public List<UserBookBean> findAllUsers2(ConnectionManager cManager) throws DBException {
		instance = cManager;
		List<UserBookBean> users = null;
		try {
			con = instance.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			users = findAllUsers2(con);

			return users;
		} catch (SQLException ex) {
			LOG.error(Constants.DBErrors.FIND_ALL_USERS, ex);
			throw new DBException(Constants.DBErrors.FIND_ALL_USERS, ex);
		} finally {
			instance.close(con);
		}
	}

	private List<UserBookBean> findAllUsers2(Connection con) throws DBException {
		List<UserBookBean> users = new ArrayList<>();
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = con.createStatement();

			rs = stmt.executeQuery(Constants.Sql.SELECT_ALL_USERS);


			while (rs.next()) {
				User user = extractUser(rs);
				UserBookBean userBean = UserBookBean.createUser(user.getUserId(), user.getLogin(), user.getPassword(), user.getEmail(), user.getName(), user.getSurname(), user.getPhone(), user.getCountry(), user.getCity(), user.getRegistrationDate(), user.isBlocked(), user.getRole());
				users.add(userBean);
			}
			return users;
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			return null;
		} finally {
			instance.close(rs);
			instance.close(stmt);
		}
	}
	
	
	@Override
	public boolean deleteUserById(ConnectionManager cManager, int userId) throws DBException {
		instance = cManager;

		try {
			con = instance.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			int res = deleteUser(con, userId);

			con.commit();
			return res != 0;
		} catch (SQLException ex) {

			instance.safeRollback(con);

			LOG.error(Constants.DBErrors.DELETE_USER, ex);
			throw new DBException(Constants.DBErrors.DELETE_USER, ex);
		} finally {
			instance.close(con);
		}
	}

	private int deleteUser(Connection con, int userId) throws DBException {
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement(Constants.Sql.DELETE_USER);
			int k = 0;
			pstmt.setInt(++k, userId);

			int res = pstmt.executeUpdate();

			return res;
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			return 0;
		} finally {
			instance.close(pstmt);
		}

	}

	private User extractUser(ResultSet rs) throws SQLException {
		User user = new User();
		user.setUserId(rs.getInt("user_id"));
		user.setLogin(rs.getString("login"));
		user.setPassword(rs.getString("password"));
		user.setEmail(rs.getString("email"));
		user.setName(rs.getString("name"));
		user.setSurname(rs.getString("surname"));
		user.setPhone(rs.getString("phone"));
		user.setCountry(rs.getString("country"));
		user.setCity(rs.getString("city"));
		user.setRegistrationDate(rs.getDate("registration_date"));
		user.setBlocked(rs.getBoolean("is_blocked"));
		String r = rs.getString("role");
		for (Role role : Role.values()) {
			if (r.equalsIgnoreCase(role.getName())) {
				user.setRole(role);
				return user;
			}
		}
		return user;
	}

}
