package ua.nure.kravchuk.SummaryTask4.repository.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.repository.dao.OrderDao;
import ua.nure.kravchuk.SummaryTask4.exception.DBException;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Order;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Status;
import ua.nure.kravchuk.SummaryTask4.repository.dao.mysql.ConnectionManager;
import ua.nure.kravchuk.SummaryTask4.util.Constants;

/**
 * Class for work with Order table. Works with MySql DB. Only the required DAO
 * methods are defined!
 * 
 * @author P.Kravchuk
 *
 */
public class MySqlOrderDao implements OrderDao {

	private static final Logger LOG = Logger.getLogger(MySqlOrderDao.class);

	private Connection con;
	private ConnectionManager instance;

	public MySqlOrderDao() {
		// no op
	}

	@Override
	public boolean makeOrder(ConnectionManager cManager, Order order) throws DBException {
		instance = cManager;
		try {
			con = instance.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			boolean res = insertOrder(con, order);

			con.commit();

			return res;
		} catch (SQLException ex) {

			instance.safeRollback(con);

			LOG.error(Constants.DBErrors.INSERT_ORDER, ex);
			throw new DBException(Constants.DBErrors.INSERT_ORDER, ex);
		} finally {
			instance.close(con);
		}
	}

	private boolean insertOrder(Connection con, Order order) throws DBException {
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement(Constants.Sql.INSERT_ORDER);
			int k = 0;
			pstmt.setDate(++k, new java.sql.Date(order.getReturnDate().getTime()));
			pstmt.setBoolean(++k, order.isInReadingRoom());
			pstmt.setString(++k, order.getStatus().getName());
			pstmt.setInt(++k, order.getUserId());
			pstmt.setInt(++k, order.getBookId());
			pstmt.executeUpdate();

			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			instance.close(pstmt);
		}

	}

	@Override
	public Order findOrderById(ConnectionManager cManager, int orderId) throws DBException {
		instance = cManager;
		Order order = null;
		try {
			con = instance.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			order = findOrderById(con, orderId);

			return order;
		} catch (SQLException ex) {
			LOG.error(Constants.DBErrors.FIND_ORDER, ex);
			throw new DBException(Constants.DBErrors.FIND_ORDER, ex);
		} finally {
			instance.close(con);
		}
	}

	private Order findOrderById(Connection con, int orderId) throws DBException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Order order = null;
		try {
			pstmt = con.prepareStatement(Constants.Sql.SELECT_ORDER_BY_ID);
			pstmt.setInt(1, orderId);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				order = extractOrder(rs);
			}

			return order;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return null;
		} finally {
			instance.close(pstmt);
		}
	}

	@Override
	public List<Order> findOrdersByUserId(ConnectionManager cManager, int userId, boolean activeOrders)
			throws DBException {
		instance = cManager;
		List<Order> orders = null;
		try {
			con = instance.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			orders = findOrdersByUserId(con, userId, activeOrders);

			return orders;
		} catch (SQLException ex) {
			LOG.error(Constants.DBErrors.FIND_ORDERS_BY_USER_ID, ex);
			throw new DBException(Constants.DBErrors.FIND_ORDERS_BY_USER_ID, ex);
		} finally {
			instance.close(con);
		}
	}

	private List<Order> findOrdersByUserId(Connection con, int userId, boolean activeOrders) throws DBException {
		List<Order> orders = new ArrayList<>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int k = 0;
		try {

			if (activeOrders) {
				pstmt = con.prepareStatement(Constants.Sql.SELECT_ACTIVE_ORDERS_BY_USER_ID);
				pstmt.setInt(++k, userId);
			} else {
				pstmt = con.prepareStatement(Constants.Sql.SELECT_ALL_ORDERS_BY_USER_ID);
				pstmt.setInt(++k, userId);
			}

			rs = pstmt.executeQuery();
			while (rs.next()) {
				Order order = extractOrder(rs);
				orders.add(order);
			}
			return orders;
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			return orders;
		} finally {
			instance.close(rs);
			instance.close(pstmt);
		}
	}

	@Override
	public boolean deleteOrderById(ConnectionManager cManager, int orderId) throws DBException {
		instance = cManager;
		try {
			con = instance.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			int res = deleteOrderById(con, orderId);
			con.commit();
			
			return res > 0;
		} catch (SQLException ex) {
			LOG.error(Constants.DBErrors.DELETE_ORDER_BY_ID, ex);
			throw new DBException(Constants.DBErrors.DELETE_ORDER_BY_ID, ex);
		} finally {
			instance.close(con);
		}
	}

	private int deleteOrderById(Connection con, int orderId) throws DBException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int k = 0;
		try {
			pstmt = con.prepareStatement(Constants.Sql.DELETE_ORDER_BY_ID);
			pstmt.setInt(++k, orderId);

			int res = pstmt.executeUpdate();

			return res;
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			return 0;
		} finally {
			instance.close(rs);
			instance.close(pstmt);
		}
	}

	@Override
	public List<Order> findAllOrders(ConnectionManager cManager, boolean orderByReturnDate) throws DBException {
		instance = cManager;
		List<Order> orders = null;
		try {
			con = instance.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			orders = findAllOrders(con, orderByReturnDate);

			return orders;
		} catch (SQLException ex) {
			LOG.error(Constants.DBErrors.FIND_ALL_ORDERS, ex);
			throw new DBException(Constants.DBErrors.FIND_ALL_ORDERS, ex);
		} finally {
			instance.close(con);
		}
	}

	private List<Order> findAllOrders(Connection con, boolean orderByReturnDate) throws DBException {
		List<Order> orders = new ArrayList<>();
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = con.createStatement();
			if (orderByReturnDate) {
				rs = stmt.executeQuery(Constants.Sql.SELECT_SORTED_ORDERS_BY_RETURN_DATE);
			} else {
				rs = stmt.executeQuery(Constants.Sql.SELECT_ALL_ORDERS);
			}

			while (rs.next()) {
				Order order = extractOrder(rs);
				orders.add(order);
			}
			return orders;
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			return null;
		} finally {
			instance.close(rs);
			instance.close(stmt);
		}
	}

	@Override
	public List<Order> findAllWaitingOrders(ConnectionManager cManager) throws DBException {
		instance = cManager;
		List<Order> orders = null;
		try {
			con = instance.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			orders = findAllWaitingOrders(con);

			return orders;
		} catch (SQLException ex) {
			LOG.error(Constants.DBErrors.FIND_ALL_ORDERS, ex);
			throw new DBException(Constants.DBErrors.FIND_ALL_ORDERS, ex);
		} finally {
			instance.close(con);
		}
	}

	private List<Order> findAllWaitingOrders(Connection con) throws DBException {
		List<Order> orders = new ArrayList<>();
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = con.createStatement();
			rs = stmt.executeQuery(Constants.Sql.SELECT_ALL_WAITING_ORDERS);

			while (rs.next()) {
				Order order = extractOrder(rs);
				orders.add(order);
			}

			return orders;
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			return null;
		} finally {
			instance.close(rs);
			instance.close(stmt);
		}
	}

	@Override
	public boolean updateOrderStatus(ConnectionManager cManager, int orderId, Status status) throws DBException {
		instance = cManager;
		try {
			con = instance.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			boolean res = updateOrderStatus(con, orderId, status) > 0;

			con.commit();

			return res;
		} catch (SQLException ex) {

			instance.safeRollback(con);

			LOG.error(Constants.DBErrors.UPDATE_ORDER_STATUS, ex);
			throw new DBException(Constants.DBErrors.UPDATE_ORDER_STATUS, ex);
		} finally {
			instance.close(con);
		}
	}

	private int updateOrderStatus(Connection con, int orderId, Status status) throws DBException {
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement(Constants.Sql.UPDATE_ORDER_STATUS);
			int k = 0;
			pstmt.setString(++k, status.getName());
			pstmt.setInt(++k, orderId);
			int res = pstmt.executeUpdate();
			con.commit();

			return res;
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			return 0;
		} finally {
			instance.close(pstmt);
		}

	}

	private Order extractOrder(ResultSet rs) throws SQLException {
		Order order = new Order();
		order.setOrderId(rs.getInt("order_id"));
		order.setReturnDate(rs.getDate("return_date"));
		order.setInReadingRoom(rs.getBoolean("is_in_reading_room"));
		order.setUserId(rs.getInt("user_id"));
		order.setBookId(rs.getInt("book_id"));
		String r = rs.getString("status");
		for (Status status : Status.values()) {
			if (r.equalsIgnoreCase(status.getName())) {
				order.setStatus(status);
				return order;
			}
		}
		return order;
	}

	
	@Override
	public List<Order> findAllOrders2(ConnectionManager cManager) throws DBException {
		instance = cManager;
		List<Order> orders = null;
		try {
			con = instance.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			orders = findAllOrders2(con);

			return orders;
		} catch (SQLException ex) {
			LOG.error(Constants.DBErrors.FIND_ALL_ORDERS, ex);
			throw new DBException(Constants.DBErrors.FIND_ALL_ORDERS, ex);
		} finally {
			instance.close(con);
		}
	}

	private List<Order> findAllOrders2(Connection con) throws DBException {
		List<Order> orders = new ArrayList<>();
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = con.createStatement();
			
			rs = stmt.executeQuery(Constants.Sql.SELECT_ALL_ORDERS_GIVEN_TO_USER);
			

			while (rs.next()) {
				Order order = extractOrder(rs);
				orders.add(order);
			}
			return orders;
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			return null;
		} finally {
			instance.close(rs);
			instance.close(stmt);
		}
	}

}
