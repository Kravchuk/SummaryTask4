package ua.nure.kravchuk.SummaryTask4.repository.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.repository.dao.OrderBookDao;
import ua.nure.kravchuk.SummaryTask4.exception.DBException;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Status;
import ua.nure.kravchuk.SummaryTask4.repository.entity.bean.OrderBookBean;
import ua.nure.kravchuk.SummaryTask4.repository.dao.mysql.ConnectionManager;
import ua.nure.kravchuk.SummaryTask4.util.Constants;

/**
 * Class for work with OrderBook table. Works with MySql DB. Only the required
 * DAO methods are defined!
 * 
 * @author P.Kravchuk
 *
 */
public class MySqlOrderBookDao implements OrderBookDao {

	private static final Logger LOG = Logger.getLogger(MySqlOrderBookDao.class);

	private Connection con;
	private ConnectionManager instance;


	public MySqlOrderBookDao() {
		// no op
	}

	@Override
	public List<OrderBookBean> findOrdersByUserId(ConnectionManager cManager, int userId, boolean isActiveOrders)
			throws DBException {
		instance = cManager;
		List<OrderBookBean> orders = null;
		try {
			con = instance.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			orders = findOrdersByUserId(con, userId, isActiveOrders);

			return orders;
		} catch (SQLException ex) {
			LOG.error(Constants.DBErrors.FIND_ORDERS_BY_USER_ID, ex);
			throw new DBException(Constants.DBErrors.FIND_ORDERS_BY_USER_ID, ex);
		} finally {
			instance.close(con);
		}
	}

	private List<OrderBookBean> findOrdersByUserId(Connection con, int userId, boolean isActiveOrders)
			throws DBException {
		List<OrderBookBean> orders = new ArrayList<>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int k = 0;
		try {
			if(!isActiveOrders){
			pstmt = con.prepareStatement(Constants.Sql.SELECT_ALL_BEAN_ORDERS_BY_USER_ID);
			pstmt.setInt(++k, userId);
			}else{
				pstmt = con.prepareStatement(Constants.Sql.SELECT_ACTIVE_BEAN_ORDERS_BY_USER_ID);
				pstmt.setInt(++k, userId);
			}

			rs = pstmt.executeQuery();
			Date currentDate = new Date();
			while (rs.next()) {
				OrderBookBean order = extractOrderBookBean(rs);
				if(currentDate.compareTo(order.getReturnDate()) > -1 && order.getStatus() == Status.GIVEN_TO_USER){
					order.setExpired(true);
				}else{
					order.setExpired(false);
				}
					orders.add(order);
			}
			
			return orders;
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			return orders;
		} finally {
			instance.close(rs);
			instance.close(pstmt);
		}
	}

	private OrderBookBean extractOrderBookBean(ResultSet rs) throws SQLException {
		OrderBookBean orderBookBean = new OrderBookBean();
		orderBookBean.setOrderId(rs.getInt("order_id"));
		orderBookBean.setReturnDate(rs.getDate("return_date"));
		orderBookBean.setInReadingRoom(rs.getBoolean("is_in_reading_room"));
		orderBookBean.setUserId(rs.getInt("user_id"));
		orderBookBean.setBookId(rs.getInt("book_id"));
		orderBookBean.setName(rs.getString("name"));
		orderBookBean.setAuthor(rs.getString("author"));
		orderBookBean.setPenaltyPayment(rs.getInt("penalty_payment"));
		String r = rs.getString("status");
		for (Status status : Status.values()) {
			if (r.equalsIgnoreCase(status.getName())) {
				orderBookBean.setStatus(status);
				return orderBookBean;
			}
		}
		return orderBookBean;
	}

}
