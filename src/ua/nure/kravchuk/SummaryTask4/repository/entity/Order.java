package ua.nure.kravchuk.SummaryTask4.repository.entity;

import java.util.Date;

/**
 * Order entity.
 *  
 * @author P.Kravchuk
 * 
 */
public class Order extends Entity{

	private static final long serialVersionUID = 1982822989248735235L;
	private int orderId;
	private Date returnDate;
	private boolean isInReadingRoom;
	private Status status;
	private int userId;
	private int bookId;

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	public boolean isInReadingRoom() {
		return isInReadingRoom;
	}

	public void setInReadingRoom(boolean isInReadingRoom) {
		this.isInReadingRoom = isInReadingRoom;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}


	@Override
	public String toString() {
		return "Order [orderId=" + orderId + ", returnDate=" + returnDate + ", isInReadingRoom=" + isInReadingRoom
				+ ", status=" + status + ", userId=" + userId + ", bookId=" + bookId + "]";
	}

	public static Order createOrder(boolean isInReadingRoom, int userId, int bookId){
		Order order = new Order();
		order.setReturnDate(new Date());
		order.setInReadingRoom(isInReadingRoom);
		order.setStatus(Status.NEW_REQUEST);
		order.setUserId(userId);
		order.setBookId(bookId);
		return order;
	}
}
