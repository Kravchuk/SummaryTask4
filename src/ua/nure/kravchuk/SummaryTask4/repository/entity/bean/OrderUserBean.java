package ua.nure.kravchuk.SummaryTask4.repository.entity.bean;

import java.util.Date;

import ua.nure.kravchuk.SummaryTask4.repository.entity.Entity;

/**
 * Provide records for virtual table:
 * 
 * <pre>
 * user.userId|user.email|order.orderId|order.returnDate|
 * </pre>
 * 
 * @author P.Kravchuk
 * 
 */
public class OrderUserBean extends Entity {

	private static final long serialVersionUID = 7826509971393408807L;

	private int userId;
	private String email;
	private int orderId;
	private Date returnDate;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	@Override
	public String toString() {
		return "OrderUserBean [userId=" + userId + ", email=" + email + ", orderId=" + orderId + ", returnDate="
				+ returnDate + "]";
	}

}
