package ua.nure.kravchuk.SummaryTask4.repository.entity.bean;

import java.util.Date;

import ua.nure.kravchuk.SummaryTask4.repository.entity.Entity;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Status;

/**
 * Provide records for virtual table:
 * <pre>
 * |order.orderId|order.userId|order.status|order.isInReadingRoom|order.returnDate|book.bookId|book.name|book.author|book.penaltyPayment|isExpired|
 * </pre>
 * 
 * @author P.Kravchuk
 * 
 */
public class OrderBookBean extends Entity {

	private static final long serialVersionUID = -6323190421963041430L;
	
	private int orderId;
	private int userId;
	private Status status;
	private boolean isInReadingRoom;
	private Date returnDate;
	private int bookId;
	private String name;
	private String author;
	private double penaltyPayment;
	private boolean isExpired;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public double getPenaltyPayment() {
		return penaltyPayment;
	}

	public void setPenaltyPayment(double penaltyPayment) {
		this.penaltyPayment = penaltyPayment;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	public boolean isInReadingRoom() {
		return isInReadingRoom;
	}

	public void setInReadingRoom(boolean isInReadingRoom) {
		this.isInReadingRoom = isInReadingRoom;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	
	public boolean isExpired() {
		return isExpired;
	}

	public void setExpired(boolean isExpired) {
		this.isExpired = isExpired;
	}

	@Override
	public String toString() {
		return "OrderBookBean [orderId=" + orderId + ", userId=" + userId + ", status=" + status + ", isInReadingRoom="
				+ isInReadingRoom + ", returnDate=" + returnDate + ", bookId=" + bookId + ", name=" + name + ", author="
				+ author + ", penaltyPayment=" + penaltyPayment + ", isExpired=" + isExpired + "]";
	}
	

}
