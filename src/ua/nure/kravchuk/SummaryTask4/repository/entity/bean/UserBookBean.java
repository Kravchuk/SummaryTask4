package ua.nure.kravchuk.SummaryTask4.repository.entity.bean;

import java.util.Date;

import ua.nure.kravchuk.SummaryTask4.repository.entity.Entity;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Role;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Status;
import ua.nure.kravchuk.SummaryTask4.repository.entity.User;

/**
 * 
 * @author P.Kravchuk
 * 
 */
public class UserBookBean extends Entity {

	private static final long serialVersionUID = -6323190421963041430L;
	
	private int userId;
	private String login;
	private String password;
	private String email;
	private String name;
	private String surname;
	private String phone;
	private String country;
	private String city;
	private Date registrationDate;
	private boolean isBlocked;
	private Role role;
	private int booksInUse;
	
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Date getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}
	public boolean isBlocked() {
		return isBlocked;
	}
	public void setBlocked(boolean isBlocked) {
		this.isBlocked = isBlocked;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public int getBooksInUse() {
		return booksInUse;
	}
	public void setBooksInUse(int booksInUse) {
		this.booksInUse = booksInUse;
	}
	
	@Override
	public String toString() {
		return "UserBookBean [userId=" + userId + ", login=" + login + ", password=" + password + ", email=" + email
				+ ", name=" + name + ", surname=" + surname + ", phone=" + phone + ", country=" + country + ", city="
				+ city + ", registrationDate=" + registrationDate + ", isBlocked=" + isBlocked + ", role=" + role
				+ ", booksInUse=" + booksInUse + "]";
	}
	
	
	public static UserBookBean createUser(int userId, String login, String password, String email, String name, String surname, String phone, String country, String city, Date registrationDate, boolean isBlocked,  Role role) {
		UserBookBean user = new UserBookBean();
		user.setUserId(userId);
		user.setLogin(login);
		user.setPassword(password);
		user.setEmail(email);
		user.setName(name);
		user.setSurname(surname);
		user.setPhone(phone);
		user.setCountry(country);
		user.setCity(city);
		user.setRegistrationDate(registrationDate);
		user.setBlocked(isBlocked);
		user.setRole(role);
		user.setBooksInUse(0);
		return user;
	}
	

}
