package ua.nure.kravchuk.SummaryTask4.repository.entity;

/**
 * Status entity.
 *  
 * @author P.Kravchuk
 * 
 */
public enum Status {
	NEW_REQUEST, GIVEN_TO_USER, RETURNED;
	
	public static Status getStatus(Order order) {
		Status status = order.getStatus();
		return status;
	}
	
	public String getName() {
		return name().toLowerCase();
	}
}
