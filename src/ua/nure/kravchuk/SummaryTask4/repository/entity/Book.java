package ua.nure.kravchuk.SummaryTask4.repository.entity;

import java.util.Date;

/**
 * Book entity.
 *  
 * @author P.Kravchuk
 * 
 */
public class Book extends Entity{

	private static final long serialVersionUID = 3696563977221523185L;
	private int bookId;
	private String name;
	private String author;
	private String publisher;
	private String genre;
	private Date publicationDate;
	private int totalAmount;
	private int available;
	private int numberOfDaysForUse;
	private double penaltyPayment;
	private int numberOfReadings;

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}


	public Date getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(Date publicationDate) {
		this.publicationDate = publicationDate;
	}

	public int getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(int totalAmount) {
		this.totalAmount = totalAmount;
	}

	public int getNumberOfDaysForUse() {
		return numberOfDaysForUse;
	}

	public void setNumberOfDaysForUse(int numberOfDaysForUse) {
		this.numberOfDaysForUse = numberOfDaysForUse;
	}

	public double getPenaltyPayment() {
		return penaltyPayment;
	}

	public void setPenaltyPayment(double penaltyPayment) {
		this.penaltyPayment = penaltyPayment;
	}


	public int getAvailable() {
		return available;
	}

	public void setAvailable(int available) {
		this.available = available;
	}

	public int getNumberOfReadings() {
		return numberOfReadings;
	}

	public void setNumberOfReadings(int numberOfReadings) {
		this.numberOfReadings = numberOfReadings;
	}



	@Override
	public String toString() {
		return "Book [bookId=" + bookId + ", name=" + name + ", author=" + author + ", publisher=" + publisher
				+ ", genre=" + genre + ", publicationDate=" + publicationDate + ", totalAmount=" + totalAmount
				+ ", avaliable=" + available + ", numberOfDaysForUse=" + numberOfDaysForUse + ", penaltyPayment="
				+ penaltyPayment + ", numberOfReadings=" + numberOfReadings + "]";
	}

	public static Book createBook(String name, String author, String publisher, String genre, Date publicationDate,
			int totalAmount, int numberOfDaysForUse, double penaltyPayment) {
		Book book = new Book();
		book.setName(name);
		book.setAuthor(author);
		book.setPublisher(publisher);
		book.setGenre(genre);
		book.setPublicationDate(publicationDate);
		book.setTotalAmount(totalAmount);
		book.setAvailable(totalAmount);
		book.setNumberOfDaysForUse(numberOfDaysForUse);
		book.setPenaltyPayment(penaltyPayment);
		book.setNumberOfReadings(0);
		return book;
	}


}
