package ua.nure.kravchuk.SummaryTask4.repository.entity;

/**
 * Role entity.
 *  
 * @author P.Kravchuk
 * 
 */
public enum Role {
	USER, ADMIN, LIBRARIAN;
	
	public static Role getRole(User user) {
		Role role = user.getRole();
		return role;
	}
	
	public String getName() {
		return name().toLowerCase();
	}
	
}
