package ua.nure.kravchuk.SummaryTask4.sevices.web;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.exception.AppException;


/**
 * Send letter to email.
 * 
 * @author P.Kravchuk
 *
 */
public class SendEmail {

	private static final Logger LOG = Logger.getLogger(SendEmail.class);

	private final String username = "testmailforproject2016@gmail.com";
	private final String password = "testmail2016";

	
	public boolean sendLetterToEmail(String toEmail, String fileName, String subject, String text) {

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, 
				new javax.mail.Authenticator() {
							protected PasswordAuthentication getPasswordAuthentication() {
									return new PasswordAuthentication(username, password);
							}
				}
		);

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
			message.setSubject(subject);
			message.setContent("<p>" + text + "</p>", "text/html");
		    //message.setFileName(fileName);
			Transport.send(message);
			LOG.trace("Message was sent successfully.");
			
			return true;
		} catch (MessagingException e) {
			LOG.error("Error while sending message to user", e);
			throw new AppException("Error while sending message to user");
		}
	}
}
