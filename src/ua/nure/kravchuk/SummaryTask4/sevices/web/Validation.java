package ua.nure.kravchuk.SummaryTask4.sevices.web;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import ua.nure.kravchuk.SummaryTask4.exception.AppException;


/**
 * Validation class.
 * 
 * @author P.Kravchuk
 * 
 */
public class Validation {

	private static final Logger LOG = Logger.getLogger(Validation.class);
	
	private Pattern other = Pattern.compile("[a-zA-Z�-��-�0-9]+");
	private Pattern phone = Pattern.compile("(\\+?[0-9]{6,12})");
	private Pattern email = Pattern.compile("^[-a-z0-9!#$%&'*+/=?^_`{|}~]+(?:\\."
			+ "[-a-z0-9!#$%&'*+/=?^_`{|}~]+)*@(?:[a-z0-9]([-a-z0-9]{0,61}"
			+ "[a-z0-9])?\\.)*(?:aero|arpa|asia|biz|cat|com|coop|edu|gov|"
			+ "info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])$");
	private Pattern name = Pattern.compile("([a-zA-Z�-��-�0-9 -:\\.,;()\\!\\?\\#\\*\\+=]+)");
	private Pattern author = Pattern.compile("([a-zA-Z�-��-�0-9 -\\.]+)");
	private Pattern genre = Pattern.compile("([a-zA-Z�-��-� -]+)");

	/**
	 * Validation for different strings
	 * 
	 * @param str string for validating
	 * @return <tt>true</tt> if string is valid
	 */
	public boolean validationOther(String str) {
		boolean res = false;
		Matcher m = other.matcher(str);
		if (m.find()) {
			res = m.group().equals(str);
		}
		return res;
	}

	
	/**
	 * Validation for book.phone number
	 * 
	 * @param str phone string for validating
	 * @return <tt>true</tt> if phone number is valid
	 */
	public boolean validationPhone(String str) {
		boolean res = false;
		Matcher m = phone.matcher(str);
		if (m.find()) {
			res = m.group().equals(str);
		}
		return res;
	}

	/**
	 * Validation for user.email
	 * 
	 * @param str email string for validating
	 * @return <tt>true</tt> if email is valid
	 */
	public boolean validationEmail(String str) {
		boolean res = false;
		Matcher m = email.matcher(str);
		if (m.find()) {
			res = m.group().equals(str);
		}
		return res;
	}
	
	/**
	 * Validation for book.name
	 * 
	 * @param str name string for validating
	 * @return <tt>true</tt> if name is valid
	 */
	public boolean validationName(String str) {
		boolean res = false;
		Matcher m = name.matcher(str);
		if (m.find()) {
			res = m.group().equals(str);
		}
		return res;
	}

	
	/**
	 * Validation for book.author
	 * 
	 * @param str author string for validating
	 * @return <tt>true</tt> if author is valid
	 */
	public boolean validationAuthor(String str) {
		boolean res = false;
		Matcher m = author.matcher(str);
		if (m.find()) {
			res = m.group().equals(str);
		}
		return res;
	}

	/**
	 * Validation for book.genre
	 * 
	 * @param str genre string for validating
	 * @return <tt>true</tt> if genre is valid
	 */
	public boolean validationGenre(String str) {
		boolean res = false;
		Matcher m = genre.matcher(str);
		if (m.find()) {
			res = m.group().equals(str);
		}
		return res;
	}
	
	
	/**
	 * Method for validating parameters from request for new user registration
	 * 
	 */
	public void validateAllParametersForNewUser(String password1, String password2, String name, String surname, String phone,
			String country, String city) {

		checkParameterOther(password1, "password1");
		checkParameterOther(password2, "password2");

		if (!password1.equals(password2)) {
			LOG.error("Password1 and password2 are different");
			throw new AppException("Password1 and password2 are different");
		}

		checkParameterName(name, "name");
		checkParameterOther(surname, "surname");
		checkParameterPhone(phone, "phone");
		checkParameterOther(country, "country");
		checkParameterOther(city, "city");
	}

	
	/** 
	 * Method for validating parameters from request for new user registration
	 * 
	 */
	public void checkParameterOther(String parameter, String message) {
		if (parameter != null && !parameter.isEmpty() && !validationOther(parameter)) {
			LOG.error("Wrong " + message);
			throw new AppException("Wrong " + message);
		}
	}

	/** 
	 * Method for validating name parameter from request for new user registration
	 * 
	 */
	public void checkParameterName(String parameter, String message) {
		if (parameter == null || parameter.isEmpty() || !validationName(parameter)) {
			LOG.error("Wrong " + message);
			throw new AppException("Wrong " + message);
		}
	}

	/** 
	 * Method for validating phone parameter from request for new user registration
	 * 
	 */
	public void checkParameterPhone(String parameter, String message) {
		if (parameter != null && !parameter.isEmpty() && !validationPhone(parameter)) {
			LOG.error("Wrong " + message);
			throw new AppException("Wrong " + message);
		}
	}

	/** 
	 * Method for validating login parameter from request for new user registration
	 * 
	 */
	public void checkParameterLogin(String parameter, String message) {
		if (parameter == null || parameter.isEmpty() || !validationOther(parameter)) {
			LOG.error("Wrong " + message);
			throw new AppException("Wrong " + message);
		}
	}

	/** 
	 * Method for validating email parameter from request for new user registration
	 * 
	 */
	public void checkParameterEmail(String parameter, String message) {
		if (parameter == null || parameter.isEmpty() || !validationEmail(parameter)) {
			LOG.error("Wrong " + message);
			throw new AppException("Wrong " + message);
		}
	}
	
	
	/**
	 * Method for validating parameters from request for existing user information editing
	 * 
	 */
	public void validateAllParametersForEditingUser(String name, String surname, String phone, String country, String city) {
		checkParameterName(name, "name");
		checkParameterOther(surname, "surname");
		checkParameterPhone(phone, "phone");
		checkParameterOther(country, "country");
		checkParameterOther(city, "city");
	}
	
	
	/**
	 * Method for validating parameters from request for new book adding
	 * 
	 */
	public void validateAllParametersForNewBook(String name, String author, String publisher, String genre, String date[],
			int totalAmount, int numberOfDaysForUse, double penaltyPayment) {
		checkParameterName(name, "name");
		checkParameterAuthor(author, "author");
		checkParameterName(publisher, "publisher");
		checkParameterGenre(genre, "genre");
		checkParameterInt(totalAmount, "total amount");
		checkParameterInt(numberOfDaysForUse, "number of days for use");
		checkParameterDouble(penaltyPayment, "penalty payment");
		checkParameterDate(date);
	}


	/**
	 * Method for validating author parameter from request for new book adding
	 * 
	 */
	public void checkParameterAuthor(String parameter, String message) {
		if (parameter == null || parameter.isEmpty() || !validationAuthor(parameter)) {
			LOG.error("Wrong " + message);
			throw new AppException("Wrong " + message);
		}
	}

	/**
	 * Method for validating genre parameter from request for new book adding
	 * 
	 */
	public void checkParameterGenre(String parameter, String message) {
		if (parameter == null || parameter.isEmpty() || !validationGenre(parameter)) {
			LOG.error("Wrong " + message);
			throw new AppException("Wrong " + message);
		}
	}

	/**
	 * Method for validating any integer parameter from request for new book adding
	 * 
	 */
	public void checkParameterInt(int number, String message) {
		if (number < 0) {
			LOG.error("Wrong " + message);
			throw new AppException("Wrong " + message);
		}
	}

	/**
	 * Method for validating any double parameter from request for new book adding
	 * 
	 */
	public void checkParameterDouble(double number, String message) {
		if (number < 0) {
			LOG.error("Wrong " + message);
			throw new AppException("Wrong " + message);
		}
	}

	/**
	 * Method for validating available parameter from request for new book adding
	 * 
	 */
	public void checkParameterAvailable(int number, int totalAmount, String message) {
		if (number < 0 || number > totalAmount) {
			LOG.error("Wrong " + message);
			throw new AppException("Wrong " + message);
		}
	}

	@SuppressWarnings("deprecation")
	public Date stringToDate(String str){
		Date date;
		String[] s;
		
		if (str != null) {
			s = str.split("-");
			date = new Date(Integer.parseInt(s[0]) - 1900, Integer.parseInt(s[1]) - 1, Integer.parseInt(s[2]));
		} else {
			LOG.error("Wrong date");
			throw new AppException("Wrong date");
		}
		
		return date;
	}

	
	/**
	 * Method for validating date parameter from request for new book adding
	 * 
	 */
	@SuppressWarnings("deprecation")
	public void checkParameterDate(String[] date) {
		int firstPrintedBookYear = 1564;
		int daysInMonths[] = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

		int year = Integer.parseInt(date[0]);
		if (year > new Date().getYear() + 1900 || year < firstPrintedBookYear) {
			LOG.error("Wrong year in date");
			throw new AppException("Wrong year in date");
		}

		int month = Integer.parseInt(date[1]);
		if (month > 12 || month < 1) {
			LOG.error("Wrong month in date");
			throw new AppException("Wrong month in date");
		}

		int day = Integer.parseInt(date[2]);
		if (day > daysInMonths[month - 1] || day < 1) {
			LOG.error("Wrong day in date");
			throw new AppException("Wrong day in date");
		}
	}
	
}

