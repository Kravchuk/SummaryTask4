package ua.nure.kravchuk.SummaryTask4.sevices.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import nl.captcha.Captcha;
import ua.nure.kravchuk.SummaryTask4.exception.AppException;
import ua.nure.kravchuk.SummaryTask4.web.recaptcha.ReCaptchaImpl;
import ua.nure.kravchuk.SummaryTask4.web.recaptcha.ReCaptchaResponse;

public class CaptchaVerifier {

	private static final Logger LOG = Logger.getLogger(CaptchaVerifier.class);

	public void offlineCaptchaCheck(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		Captcha captcha = (Captcha) session.getAttribute(Captcha.NAME);
		String answer = request.getParameter("answer");

		if (!captcha.isCorrect(answer)) {
			LOG.debug("Invalid captcha from user while sign in");
			throw new AppException("Invalid recaptcha, thy again");
		} else {
			LOG.debug("Captcha is correct");
		}
	}

	public void onlineCaptchaCheck(HttpServletRequest request, HttpServletResponse response) {
		String remoteAddr = request.getRemoteAddr();
		ReCaptchaImpl reCaptcha = new ReCaptchaImpl();
		reCaptcha.setPrivateKey("6LczdikTAAAAAKjlPwKC3o_Za4wBToxL46izmquk");

		String challenge = request.getParameter("recaptcha_challenge_field");
		String uresponse = request.getParameter("recaptcha_response_field");

		ReCaptchaResponse reCaptchaResponse = reCaptcha.checkAnswer(remoteAddr, challenge, uresponse);

		if (!reCaptchaResponse.isValid()) {
			LOG.debug("Invalid recaptcha from user while sign in");
			throw new AppException("Invalid recaptcha, thy again");
		} else {
			LOG.debug("Recaptcha is correct");
		}
	}
}
