package ua.nure.kravchuk.SummaryTask4.sevices;

import java.util.List;

import ua.nure.kravchuk.SummaryTask4.repository.dao.mysql.ConnectionManager;
import ua.nure.kravchuk.SummaryTask4.repository.dao.mysql.MySqlDaoFactory;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Order;
import ua.nure.kravchuk.SummaryTask4.repository.entity.User;
import ua.nure.kravchuk.SummaryTask4.repository.entity.bean.UserBookBean;


/**
 * Class for access from servlets to users table in database
 * 
 * @author P.Kravchuk
 *
 */

public class UserService {
	private static ConnectionManager manager = new ConnectionManager();
	private static MySqlDaoFactory daoFactory = MySqlDaoFactory.getInstance();


	public boolean insertUser(User user) {
		boolean result = daoFactory.getUserDao().insertUser(manager, user);
		return result;
	}

	public boolean deleteUser(int userId){
		boolean result = daoFactory.getUserDao().deleteUserById(manager, userId);
		return result;
	}
	
	public boolean updateUser(User user){
		boolean result = daoFactory.getUserDao().updateUser(manager, user);
		return result;
	}
	
	 public boolean updateUserIsBlocked(int userId, boolean isBlocked){
		boolean result = daoFactory.getUserDao().updateUserIsBlocked(manager, userId, isBlocked);
		return result;
	 }

	public User findUserByLogin(String login) {
		User user = daoFactory.getUserDao().findUserByLoginOrEmailAndPassword(manager, login, null, null);
		return user;
	}

	public User findUserByEmail(String email) {
		User user = daoFactory.getUserDao().findUserByLoginOrEmailAndPassword(manager, null, email, null);
		return user;
	}
	
	public User findUserById(int id) {
		User user = daoFactory.getUserDao().findUserById(manager, id);
		return user;
	}

	public List<User> findUsers(boolean isLibrarian) {
		List<User> users = daoFactory.getUserDao().findAllUsers(manager, isLibrarian);
		return users;
	}
	
	
	public List<UserBookBean> findUsersTest(){
		List<UserBookBean> users = daoFactory.getUserDao().findAllUsers2(manager);
		List<Order> orders = daoFactory.getOrderDao().findAllOrders2(manager);

		for (int i = 0; i < users.size(); i++) {
			UserBookBean user = users.get(i);
			for (int j = 0; j < orders.size(); j++) {
				if(orders.get(j).getUserId() == user.getUserId()){
					users.remove(i);
					int booksInUse = user.getBooksInUse()+1;
					user.setBooksInUse(booksInUse);
					users.add(i, user);
				}
			}
		}
		return users;
	}
}
