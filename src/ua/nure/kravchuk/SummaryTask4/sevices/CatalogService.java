package ua.nure.kravchuk.SummaryTask4.sevices;

import java.util.List;


import ua.nure.kravchuk.SummaryTask4.repository.dao.mysql.ConnectionManager;
import ua.nure.kravchuk.SummaryTask4.repository.dao.mysql.MySqlDaoFactory;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Book;


/**
 * Class for access from servlets to catalog table in database
 * 
 * @author P.Kravchuk
 *
 */

public class CatalogService {
	private static ConnectionManager manager = new ConnectionManager();
	private static MySqlDaoFactory daoFactory = MySqlDaoFactory.getInstance();
	

	public List<Book> getCatalog(String orderByParameter){
		List<Book> books = daoFactory.getCatalogDao().findAllBooks(manager,orderByParameter);
		return books;
	}
	
	
	public Book findBookById(int bookId){
		Book book = daoFactory.getCatalogDao().findBookById(manager, bookId);
		return book;
	}
	
	public List<Book> findBookByParameters(String name, String author, String publisher, String genre, String date, String orderBy, boolean reverse){
		List<Book> books = daoFactory.getCatalogDao().findBooksByParameters(manager, name, author, publisher, genre, date, orderBy, reverse);
		return books;
	}
	
	
	public boolean deleteBookById(int bookId){
		boolean result = daoFactory.getCatalogDao().deleteBookById(manager, bookId);
		return result;
	}
	
	public boolean updateBook(int bookId, Book book){
		boolean result = daoFactory.getCatalogDao().updateBook(manager, bookId, book);
		return result;
	}
	
	public boolean insertBook(Book book){
		boolean result = daoFactory.getCatalogDao().insertBook(manager, book);
		return result;
	}
	
}
