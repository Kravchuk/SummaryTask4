package ua.nure.kravchuk.SummaryTask4.sevices;

import java.util.Date;
import java.util.List;

import ua.nure.kravchuk.SummaryTask4.repository.dao.mysql.ConnectionManager;
import ua.nure.kravchuk.SummaryTask4.repository.dao.mysql.MySqlDaoFactory;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Book;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Order;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Status;
import ua.nure.kravchuk.SummaryTask4.repository.entity.User;
import ua.nure.kravchuk.SummaryTask4.repository.entity.bean.OrderBookBean;
import ua.nure.kravchuk.SummaryTask4.repository.entity.bean.OrderUserBean;


/**
 * Class for access from servlets to orders table in database
 * 
 * @author P.Kravchuk
 *
 */

public class OrderService {
	private static ConnectionManager manager = new ConnectionManager();
	private static MySqlDaoFactory daoFactory = MySqlDaoFactory.getInstance();


	public List<Order> getUserOrders(User user) {
		List<Order> orders = daoFactory.getOrderDao().findOrdersByUserId(manager, user.getUserId(), false);
		return orders;
	}

	public boolean makeOrder(Order order) {
		CatalogService managerCat = new CatalogService();
		Book book = managerCat.findBookById(order.getBookId());
		if (book != null && book.getAvailable() > 0) {
			long currentTime = System.currentTimeMillis();
			long timeToRead = 0;
			long millisInDay = 86_400_000;
			if(!order.isInReadingRoom()){
				timeToRead = book.getNumberOfDaysForUse() * millisInDay;
			}else{
				timeToRead = millisInDay;
			}
			order.setReturnDate(new Date(currentTime + timeToRead));
		} else {
			return false;
		}
		
		boolean result = daoFactory.getOrderDao().makeOrder(manager, order);
		
		return result;
	}

	public List<Order> getUnconfirmedOrders() {
		List<Order> orders = daoFactory.getOrderDao().findAllWaitingOrders(manager);
		return orders;
	}
	
	public List<Order> getAllOrders() {
		List<Order> orders = daoFactory.getOrderDao().findAllOrders(manager, true);
		return orders;
	}

	public boolean updateOrderStatus(int orderId, Status status) {
		boolean result = daoFactory.getOrderDao().updateOrderStatus(manager, orderId, status);
		
		if (result && status.equals(Status.RETURNED)) {
			Order order = findOrderById(orderId);
			
			if (order != null) {
				CatalogService managerCat = new CatalogService();
				Book book = managerCat.findBookById(order.getBookId());
				
				if (book != null) {
					book.setAvailable(book.getAvailable() + 1);
					book.setNumberOfReadings(book.getNumberOfReadings() + 1);
					result = result && managerCat.updateBook(book.getBookId(), book);
				}
				
			}
		}
		return result;
	}

	public Order findOrderById(int orderId) {
		Order order = daoFactory.getOrderDao().findOrderById(manager, orderId);
		return order;
	}
	
	public boolean deleteOrderById(int orderId) {
		boolean result = daoFactory.getOrderDao().deleteOrderById(manager, orderId);
		return result;
	}

	public List<OrderBookBean> findBeanOrdersByUserId(int userId) {
		List<OrderBookBean> orders = daoFactory.getOrderBookDao().findOrdersByUserId(manager, userId, false);
		return orders;
	}
	
	public List<OrderUserBean> findAllEmailsFromExpiredOrders(){
		List<OrderUserBean> emails = daoFactory.getOrderUserDao().findAllEmailsFromExpiredOrders(manager);
		return emails;
	}
}
