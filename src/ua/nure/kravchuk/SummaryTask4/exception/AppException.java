package ua.nure.kravchuk.SummaryTask4.exception;

/**
 * An exception that provides information on an application error.
 * 
 * @author P.Kravchuk
 * 
 */
public class AppException extends RuntimeException {

	private static final long serialVersionUID = -3440258606789990487L;

	public AppException() {
		super();
	}

	public AppException(String message, Throwable cause) {
		super(message, cause);
	}

	public AppException(String message) {
		super(message);
	}
}
