package ua.nure.kravchuk.SummaryTask4.exception;

/**
 * An exception that provides information on a database access error.
 * 
 * @author P.Kravchuk
 * 
 */
public class DBException extends AppException {

	private static final long serialVersionUID = -6346525825717496053L;

	public DBException() {
		super();
	}
	
	public DBException(String message, Throwable cause) {
		super(message, cause);
	}

	public DBException(String message) {
		super(message);
	}
}
