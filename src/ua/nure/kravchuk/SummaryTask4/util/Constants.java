package ua.nure.kravchuk.SummaryTask4.util;

/**
 * Constant messages holder (jsp pages, controller commands, sql commands, error messages).
 * 
 * @author P.Kravchuk
 * 
 */
public final class Constants {

	
	/**
	 * Class for holding paths to jsp pages
	 *
	 */
	public static class Path {
		
		// pages
		public static final String CATALOG     		 	= "WEB-INF/jsp/catalog.jsp";
		public static final String USERS 			 	= "WEB-INF/jsp/users.jsp";
		public static final String ADD_BOOK 		 	= "WEB-INF/jsp/add_book.jsp";
		public static final String ORDERS 			 	= "WEB-INF/jsp/orders.jsp";
		public static final String UNCONFIRMED_ORDERS 	= "WEB-INF/jsp/unconfirmed_orders.jsp";
		public static final String USER_ORDERS 			= "WEB-INF/jsp/user_orders.jsp";
		public static final String USER_PROFILE 		= "WEB-INF/jsp/user_profile_view.jsp";
		public static final String EDIT_USER_PROFILE	= "WEB-INF/jsp/user_profile_edit.jsp";
		public static final String EXPIRED_ORDERS 		= "WEB-INF/jsp/expired_orders.jsp";
		public static final String BOOK_PROFILE 		= "WEB-INF/jsp/book_profile.jsp";
		public static final String ERROR 				= "WEB-INF/jsp/error_page.jsp";
		public static final String CART 				= "WEB-INF/jsp/cart_final.jsp";
		public static final String BEGIN 				= "/index.jsp";
		
		
		public static final String SEND_REDIRECT = "sendRedirect";
		public static final String REDIRECT_PAGE = "redirected.jsp";

	}
	
	/**
	 * Class for holding application errors
	 *
	 */
	public static class AppErrors{
		
		public static final String EMPTY_LOGIN_PASSWORD		= "Login/password cannot be empty";
		public static final String FIND_USER 				= "Cannot find user with such login/password";
		public static final String NO_ACCESS_RIGHTS			= "You do not have permission to access the requested resource";
		public static final String NOT_LOGGED_IN 			= "You have no access rights";	
		
	}
	
	
	/**
	 * Class for holding database errors
	 *
	 */
	public static class DBErrors {

		//user errors
		public static final String INSERT_USER 				= "Cannot add a user";
		public static final String UPDATE_USER 				= "Cannot update a user";
		public static final String UPDATE_USER_IS_BLOCKED	= "Cannot update a user isBlocked cell";
		public static final String FIND_USER 				= "Cannot obtain a user";
		public static final String FIND_ALL_USERS 			= "Cannot find all users";
		public static final String DELETE_USER 				= "Cannot delete a user";

		//catalog errors
		public static final String INSERT_BOOK 				= "Cannot add a book";
		public static final String UPDATE_BOOK 				= "Cannot update a book";
		public static final String DELETE_BOOK 				= "Cannot delete a book";
		public static final String FIND_BOOK_BY_ID			= "Cannot find a book by id";
		public static final String FIND_BOOKS_BY_PRAMETERS 	= "Cannot find books by parameters";
		public static final String FIND_ALL_BOOKS 			= "Cannot find all books";

		//order errors
		public static final String INSERT_ORDER 			= "Cannot add an order";
		public static final String FIND_ORDER 				= "Cannot find an order";
		public static final String FIND_ORDERS_BY_USER_ID 	= "Cannot obtain orders by user id";
		public static final String DELETE_ORDER_BY_ID	 	= "Cannot delete order by order id";
		public static final String FIND_ALL_ORDERS			= "Cannot obtain  all orders";
		public static final String UPDATE_ORDER_STATUS 		= "Cannot update order status";

		//connection errors
		public static final String INITIALIZE_DATASOURCE 	= "Couldn't initialize datasource";
		public static final String INITIALIZE_CONTEXT 		= "Couldn't initialize context";
		public static final String OBTAIN_CONNECTION 		= "Cannot obtain a connection from the pool";
		public static final String ROLLBACK_CONNECTION		= "Cannot rollback a connection";
		public static final String CLOSE_CONNECTION			= "Cannot close a connection";
		public static final String CLOSE_RESULTSET 			= "Cannot close a result set";
		public static final String CLOSE_STATEMENT 			= "Cannot close a statement";
		
	}

	/**
	 * Class for holding sql constants
	 *
	 */
	public static class Sql {
		
		//catalog commands
		public static final String INSERT_BOOK 									= "INSERT INTO `library`.`catalog` VALUES (DEFAULT, ?, ?, ?, ?, ?, ?, ?, ?, ?, DEFAULT)";
		public static final String UPDATE_BOOK 									= "UPDATE `library`.`catalog` SET name=?, author=?, publisher=?, genre=?, publication_date=?, total_amount=?, available=?, number_of_days_for_use=?, penalty_payment=?, number_of_readings=? WHERE book_id=?";
		public static final String DELETE_BOOK_BY_ID 							= "DELETE FROM `library`.`catalog` WHERE book_id=?";
		public static final String SELECT_BOOK_BY_ID 							= "SELECT * FROM `library`.`catalog` WHERE book_id=?";
		public static final String SELECT_BOOK_BY_NAME 							= "SELECT * FROM `library`.`catalog` WHERE name=?";
		public static final String SELECT_BOOK_BY_AUTHOR 						= "SELECT * FROM `library`.`catalog` WHERE author=?";
		public static final String SELECT_BOOK_BY_NAME_AND_AUTHOR				= "SELECT * FROM `library`.`catalog` WHERE name=? AND author=?";
		public static final String SELECT_BOOK_BY_PARAMETERS 					= "SELECT * FROM `library`.`catalog` WHERE ";
		public static final String SELECT_CATLOG 								= "SELECT * FROM `library`.`catalog`";
		public static final String SELECT_SORTED_CATLOG_BY_POPULARITY 			= "SELECT * FROM `library`.`catalog` ORDER BY number_of_readings";
		public static final String SELECT_SORTED_CATLOG_BY_NAME 				= "SELECT * FROM `library`.`catalog` ORDER BY name";
		public static final String SELECT_SORTED_CATLOG_BY_AUTHOR 				= "SELECT * FROM `library`.`catalog` ORDER BY author";
		public static final String SELECT_SORTED_CATLOG_BY_PUBLISHER 			= "SELECT * FROM `library`.`catalog` ORDER BY publisher";
		public static final String SELECT_SORTED_CATLOG_BY_PUBLICATION_DATE 	= "SELECT * FROM `library`.`catalog` ORDER BY publication_date";

		//order commands
		public static final String INSERT_ORDER 								= "INSERT INTO `library`.`orders` VALUES (DEFAULT, ?, ?, ?, ?, ?)";
		public static final String SELECT_ORDER_BY_ID 							= "SELECT * FROM `library`.`orders` WHERE order_id=?";
		public static final String SELECT_ALL_ORDERS_BY_USER_ID 				= "SELECT * FROM `library`.`orders` WHERE user_id=?";
		public static final String SELECT_ACTIVE_ORDERS_BY_USER_ID 				= "SELECT * FROM `library`.`orders` WHERE user_id=? AND status='given_to_user'";
		public static final String SELECT_EMAILS_OF_USERS_WITH_EXPIRED_ORDERS 	= "SELECT  `library`.`users`.`user_id`, `library`.`users`.`email`, `library`.`orders`.`order_id`, `library`.`orders`.`return_date`  FROM `library`.`users` JOIN `library`.`orders` WHERE `library`.`orders`.`status`='given_to_user' AND `library`.`orders`.`user_id`=`library`.`users`.`user_id`";
		public static final String SELECT_ALL_ORDERS 							= "SELECT * FROM `library`.`orders`";
		public static final String SELECT_ALL_WAITING_ORDERS 					= "SELECT * FROM `library`.`orders` WHERE status='new_request'";
		public static final String SELECT_SORTED_ORDERS_BY_RETURN_DATE			= "SELECT * FROM `library`.`orders` ORDER BY return_date";
		public static final String DELETE_ORDER_BY_ID							= "DELETE FROM `library`.`orders`  WHERE order_id=?";
		public static final String UPDATE_ORDER_STATUS 							= "UPDATE `library`.`orders` SET  status=? WHERE order_id=?";

		//user commands
		public static final String INSERT_USER 									= "INSERT INTO `library`.`users` VALUES (DEFAULT, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		public static final String UPDATE_USER 									= "UPDATE `library`.`users` SET login=?, name=?, surname=?, phone=?, country=?, city=? WHERE user_id=?";
		public static final String UPDATE_USER_IS_BLOCKED 						= "UPDATE `library`.`users` SET  is_blocked=? WHERE user_id=?";
		public static final String SELECT_USER_BY_LOGIN_AND_PASSWORD 			= "SELECT * FROM `library`.`users` WHERE login=? AND password=?";
		public static final String SELECT_USER_BY_EMAIL_AND_PASSWORD 			= "SELECT * FROM `library`.`users` WHERE email=? AND password=?";
		public static final String SELECT_USER_BY_LOGIN 						= "SELECT * FROM `library`.`users` WHERE login=?";
		public static final String SELECT_USER_BY_EMAIL 						= "SELECT * FROM `library`.`users` WHERE email=?";
		public static final String SELECT_USER_BY_ID 							= "SELECT * FROM `library`.`users` WHERE user_id=?";
		public static final String SELECT_ALL_USERS 							= "SELECT * FROM `library`.`users` WHERE role='user'";
		public static final String SELECT_ALL_LIBRARIANS 						= "SELECT * FROM `library`.`users` WHERE role='librarian'";
		public static final String DELETE_USER 									= "DELETE FROM `library`.`users` WHERE user_id=?";

		
		//order book bean commands
		public static final String SELECT_ALL_BEAN_ORDERS_BY_USER_ID 	= "SELECT `library`.`orders`.*, `library`.`catalog`.`name`, `library`.`catalog`.`author`, `library`.`catalog`.`penalty_payment` FROM catalog JOIN orders WHERE catalog.book_id=orders.book_id AND orders.user_id=?";
		public static final String SELECT_ACTIVE_BEAN_ORDERS_BY_USER_ID = "SELECT `library`.`orders`.*, `library`.`catalog`.`name`, `library`.`catalog`.`author`, `library`.`catalog`.`penalty_payment` FROM catalog JOIN orders WHERE catalog.book_id=orders.book_id AND orders.user_id=? AND status='given_to_user'";
		
		
		//test
		public static final String SELECT_ALL_ORDERS_GIVEN_TO_USER	= "SELECT * FROM orders  where orders.status='given_to_user'";
		//public static final String SELECT_ALL_USERS_GIVEN_TO_USER	= "SELECT  `library`.`users`.* FROM `library`.`users` JOIN `library`.`orders` WHERE `library`.`orders`.`status`='given_to_user' AND `library`.`orders`.`user_id`=`library`.`users`.`user_id`";
	}
	
	/**
	 * Class for holding application constants
	 *
	 */
	public static class AppConsts {
		
		public static final int MAX_SIZE_OF_CART 		= 10;
		
	}
	
	
}
