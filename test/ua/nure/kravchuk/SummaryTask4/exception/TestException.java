package ua.nure.kravchuk.SummaryTask4.exception;

import org.junit.Test;

public class TestException {

	@Test
	public void testAppEx() {
		new AppException();
		new AppException("m");
		new AppException("m", new Exception());
	}
	
	
	@Test
	public void testDBEx() {
		new DBException();
		new DBException("m");
		new DBException("m", new Exception());
	}

}
