package ua.nure.kravchuk.SummaryTask4;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import ua.nure.kravchuk.SummaryTask4.exception.TestException;
import ua.nure.kravchuk.SummaryTask4.repository.dao.mysql.TestRepository;
import ua.nure.kravchuk.SummaryTask4.repository.entity.TestEntity;
import ua.nure.kravchuk.SummaryTask4.repository.entity.bean.TestBean;
import ua.nure.kravchuk.SummaryTask4.services.TestServices;
import ua.nure.kravchuk.SummaryTask4.util.TestUtil;
import ua.nure.kravchuk.SummaryTask4.web.TestWeb;
import ua.nure.kravchuk.SummaryTask4.web.command.catalog.TestCatalogCommands;
import ua.nure.kravchuk.SummaryTask4.web.command.order.TestOrderCommands;
import ua.nure.kravchuk.SummaryTask4.web.command.user.TestUserCommands;


@RunWith(Suite.class)
@SuiteClasses({ TestEntity.class, TestBean.class, TestUtil.class, TestException.class, TestServices.class,
		TestRepository.class, TestWeb.class, TestCatalogCommands.class, TestOrderCommands.class,
		TestUserCommands.class})
public class AllTests {

}
