package ua.nure.kravchuk.SummaryTask4.repository.entity;

import static org.junit.Assert.*;

import java.sql.Date;

import org.junit.Test;


public class TestEntity {

	@Test
	public void testEntity() {
		Entity entity = new Entity();
		long id = 2222;
		entity.setId(id);
		long eId = entity.getId();
		assertEquals(id, eId);
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testBook() {
		Book book = new Book();
		book.setBookId(1);
		book.setAuthor("a");
		book.setAvailable(1);
		book.setGenre("g");
		book.setName("n");
		book.setNumberOfDaysForUse(2);
		book.setNumberOfReadings(3);
		book.setPublicationDate(new Date(116, 9, 9));
		book.setPenaltyPayment(3.0);
		book.setPublisher("p");
		book.setTotalAmount(4);
		
		Book book2 = Book.createBook(book.getName(), book.getAuthor(), book.getPublisher(), book.getGenre(), book.getPublicationDate(), book.getTotalAmount(), book.getNumberOfDaysForUse(), book.getPenaltyPayment());
		book2.setBookId(book.getBookId());
		book2.setAvailable(book.getAvailable());
		book2.setNumberOfReadings(book.getNumberOfReadings());
		
		assertEquals(book.toString(), book2.toString());
	}
	
	@Test
	public void testUser(){
		User user = new User();
		user.setBlocked(true);
		user.setCity("c");
		user.setCountry("co");
		user.setEmail("e");
		user.setLogin("l");
		user.setName("n");
		user.setPassword("p");
		user.setPhone("ph");
		user.setRegistrationDate(new Date(116, 9, 9));
		user.setRole(Role.USER);
		user.setSurname("s");
		user.setUserId(1);
		
		User user2 = User.createUser(user.getLogin(), user.getPassword(), user.getEmail(), user.getName(), user.getSurname(), user.getPhone(), user.getCountry(), user.getCity(), user.getRole());
		user2.setBlocked(user.isBlocked());
		user2.setRegistrationDate(user.getRegistrationDate());
		user2.setUserId(user.getUserId());
		
		assertEquals(user.toString(), user2.toString());
	}
	
	@Test
	public void testOrder(){
		Order order = new Order();
		order.setBookId(1);
		order.setInReadingRoom(true);
		order.setOrderId(2);
		order.setReturnDate(new Date(116, 9, 9));
		order.setStatus(Status.GIVEN_TO_USER);
		order.setUserId(3);
		
		Order order2 = Order.createOrder(order.isInReadingRoom(), order.getUserId(), order.getBookId());
		order2.setOrderId(order.getOrderId());
		order2.setReturnDate(order.getReturnDate());
		order2.setStatus(order.getStatus());
		
		assertEquals(order.toString(), order2.toString());
	}
	
	@Test
	public void testRole(){
		Role role = Role.ADMIN;
		Role.valueOf("USER");
		User user = new User();
		user.setRole(Role.ADMIN);
		assertEquals(role.getName(), Role.getRole(user).toString().toLowerCase());
	}
	
	@Test
	public void testStatus(){
		Status status = Status.GIVEN_TO_USER;
		Status.valueOf("RETURNED");
		Order order = new Order();
		order.setStatus(Status.GIVEN_TO_USER);
		assertEquals(status.getName(), Status.getStatus(order).toString().toLowerCase());
	}
	
}
