package ua.nure.kravchuk.SummaryTask4.repository.entity.bean;

import static org.junit.Assert.*;

import java.sql.Date;

import org.junit.Test;

import ua.nure.kravchuk.SummaryTask4.repository.entity.Order;
import ua.nure.kravchuk.SummaryTask4.repository.entity.Status;

public class TestBean {

	@Test
	public void testOrderBookBean() {
		OrderBookBean bean = new OrderBookBean();
		bean.setBookId(1);
		bean.setInReadingRoom(true);
		bean.setOrderId(2);
		bean.setReturnDate(new Date(116, 9, 9));
		bean.setStatus(Status.GIVEN_TO_USER);
		bean.setUserId(3);
		bean.setAuthor("a");
		bean.setName("n");
		bean.setPenaltyPayment(3.0);
		bean.setExpired(true);
		
		OrderBookBean bean2 = new OrderBookBean();
		bean2.setBookId(bean.getBookId());
		bean2.setInReadingRoom(bean.isInReadingRoom());
		bean2.setOrderId(bean.getOrderId());
		bean2.setReturnDate(bean.getReturnDate());
		bean2.setStatus(bean.getStatus());
		bean2.setUserId(bean.getUserId());
		bean2.setAuthor(bean.getAuthor());
		bean2.setName(bean.getName());
		bean2.setPenaltyPayment(bean.getPenaltyPayment());
		bean2.setExpired(bean.isExpired());
		
		assertEquals(bean.toString(), bean2.toString());
	}

}
