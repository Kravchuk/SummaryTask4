package ua.nure.kravchuk.SummaryTask4.repository.dao.mysql;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;

import ua.nure.kravchuk.SummaryTask4.util.Constants;


public class TestRepository {

	@Test
	public void testConnectionManager()
	{
	}
	
	@Test
	public void testMySqlDAOFactory()
	{
		MySqlDaoFactory d = new MySqlDaoFactory();
		d.getCatalogDao();
		d.getOrderBookDao();
		d.getOrderDao();
		d.getUserDao();
	}

}
