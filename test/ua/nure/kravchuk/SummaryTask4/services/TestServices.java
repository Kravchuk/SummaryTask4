package ua.nure.kravchuk.SummaryTask4.services;

import org.junit.Test;

import ua.nure.kravchuk.SummaryTask4.repository.entity.Book;
import ua.nure.kravchuk.SummaryTask4.sevices.CatalogService;
import ua.nure.kravchuk.SummaryTask4.sevices.OrderService;
import ua.nure.kravchuk.SummaryTask4.sevices.UserService;
import ua.nure.kravchuk.SummaryTask4.sevices.web.SendEmail;
import ua.nure.kravchuk.SummaryTask4.sevices.web.Validation;

import static org.junit.Assert.*;

public class TestServices {

	@Test
	public void testSendEamil() {
		SendEmail email = new SendEmail();
		
		assertTrue(email.sendLetterToEmail("kpaveld@gmail.com", "Test", "Test", "test"));
	}
	
	@Test
	public void testValiadtion() {
		Validation v = new Validation();
		
		assertTrue(v.validationAuthor("author"));
		assertTrue(v.validationName("name"));
		assertTrue(v.validationOther("other"));
		assertTrue(v.validationPhone("1234567"));
		assertTrue(v.validationEmail("mail@mail.com"));
		assertTrue(v.validationGenre("genre"));
		
		
	}
	
	@Test
	public void testCatalogService() {
	}
	
	@Test
	public void testOrderService() {
	}
	
	@Test
	public void testUserService() {
	}

}
