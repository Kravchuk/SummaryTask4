package ua.nure.kravchuk.SummaryTask4.web.command.order;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestOrderCommands {

	@Test
	public void testOrderCommands() {
		new AddToCartCommand();
		new CancelOrderCommand();
		new DeleteFromCartCommand();
		new GetAllOrdersCommand();
		new GetUnconfirmedOrdersCommand();
		new GetUserOrdersCommand();
		new GiveBookCommand();
		new MakeOrderCommand();
		new PayPenaltyCommand();
		new ReturnBookCommand();
	}

}
