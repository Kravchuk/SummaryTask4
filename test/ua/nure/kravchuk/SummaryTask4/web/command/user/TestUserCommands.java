package ua.nure.kravchuk.SummaryTask4.web.command.user;


import java.io.IOException;

import javax.servlet.ServletException;

import org.junit.Test;

import ua.nure.kravchuk.SummaryTask4.exception.AppException;

public class TestUserCommands {
	
	
	@Test
	public void testUserCommands() throws AppException, IOException, ServletException {
		new AddNewLibrarianCommand();
		new BlockUserCommand();
		new DeleteLibrarianCommand();
		new FindUserCommand();
		new GetUsersCommand();
		new SendNotificationsCommand();
	}

}
