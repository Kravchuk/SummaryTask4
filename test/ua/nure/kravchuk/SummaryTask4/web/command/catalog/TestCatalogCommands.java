package ua.nure.kravchuk.SummaryTask4.web.command.catalog;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestCatalogCommands {

	@Test
	public void testCatalogCommands() {
		new AddNewBookCommand();
		new DeleteBookCommand();
		new EditBookCommand();
		new FindBookCommand();
		new FindBooksByParametersCommand();
		new GetCatalogCommand();
	}

}
