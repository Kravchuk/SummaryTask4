package ua.nure.kravchuk.SummaryTask4.web;

import static org.junit.Assert.*;

import javax.servlet.jsp.JspException;

import org.junit.Test;

import ua.nure.kravchuk.SummaryTask4.web.command.Command;
import ua.nure.kravchuk.SummaryTask4.web.command.CommandContainer;
import ua.nure.kravchuk.SummaryTask4.web.command.LoginCommand;
import ua.nure.kravchuk.SummaryTask4.web.command.LogoutCommand;
import ua.nure.kravchuk.SummaryTask4.web.command.NoCommand;
import ua.nure.kravchuk.SummaryTask4.web.command.RegistrationCommand;
import ua.nure.kravchuk.SummaryTask4.web.filter.CommandAccessFilter;
import ua.nure.kravchuk.SummaryTask4.web.filter.EncodingFilter;
import ua.nure.kravchuk.SummaryTask4.web.listener.ContextListener;
import ua.nure.kravchuk.SummaryTask4.web.tag.custom.ChangePhoneTag;

public class TestWeb {

	@Test
	public void testWeb() {
		new Controller();
		new CommandContainer();
		new LoginCommand();
		new LogoutCommand();
		new NoCommand();
		new RegistrationCommand();
	}
	
	@Test
	public void testFilters(){
		new CommandAccessFilter();
		new EncodingFilter();
	}

	@Test
	public void testListeners(){
		new ContextListener();
	}
	
	@Test
	public void testTags() throws JspException{
		ChangePhoneTag changePhoneTag = new ChangePhoneTag();
		changePhoneTag.setMyPhone("0972335147");
	}
}


